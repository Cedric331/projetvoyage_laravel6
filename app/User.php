<?php

namespace App;

use Illuminate\Support\Facades\Auth;
use Cmgmyr\Messenger\Traits\Messagable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Notifications\DatabaseNotification;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends \TCG\Voyager\Models\User implements MustVerifyEmail
{
    use Messagable;
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'pseudo', 'email', 'avatar', 'cgu', 'password' ,'color', 'email_verified_at','is_admin'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token', 'update_at',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * Renvoi les commentaires de l'utilisateur
     *
     * @return void
     */
    public function commentaires()
    {
        return $this->hasMany(CommentairePays::class);
    }

    /**
     * Retourne les demandes d'amis non acceptée de l'utilisateur
     *
     * @return void
     */
    public function demandeAmis()
    {
        return $this->hasMany(Amis::class)
                ->where('demande', '=', false)
                ->join('users', 'users.id', '=', 'amis.amis')
                ->select('pseudo','amis.id', 'amis.user_id');
    }

    /**
     * Retourne les amis de l'utilisateur
     *
     * @return void
     */
    public function amis()
    {
        return $this->hasMany(Amis::class)
                    ->where('demande', '=', true)
                    ->join('users', 'users.id', '=', 'amis.amis');
    }

    /**
     *  Vérifie si une demande d'amis entre les deux utilisateurs est en cours
     *
     * @return void
     */
    public function verificationDemandeAmis($user)
    {
        return $this->hasMany(Amis::class)
                ->where('demande', '=', false)
                ->where('amis', '=', $user)
                ->get();
    }

    /**
     * Vérifie si les utilisateurs sont amis
     *
     * @return void
     */
    public function verificationAmis($user)
    {
        return $this->hasMany(Amis::class)
                    ->where('demande', '=', true)
                    ->where('amis', '=', $user)
                    ->get();
    }


    /**
     * Retourne les expériences de l'utilisateur
     *
     * @return void
     */
    public function experience()
    {
        return $this->hasMany(Experience::class)
                ->join('users', 'users.id', '=', 'experiences.user_id')
                ->join('pays', 'pays.id', '=', 'experiences.pays_id')
                ->select('experiences.id', 'titre_experience', 'description_experience', 'note_experience', 'nom_pays', 'pseudo', 'avatar', 'experiences.user_id');
    }

    /**
     * Retourne les annonces postés par l'utilisateur
     *
     * @return void
     */
    public function annonces()
    {
        return $this->hasMany(Annonces::class);
    }

    /**
     * Vérifie si le pays est dans la liste des favoris de l'utilisateur
     *
     * @param [type] $pays contient l'id du pays
     * @return boolean
     */
    public function isFavoris($pays)
    {
        $favoris = $this->hasMany(Favoris::class)
                    ->where('pays_id', $pays)
                    ->where('user_id', Auth::user()->id)
                    ->first();

        if (empty($favoris))
        {
            return false;
        }
        else {
            return true;
        }
    }

    /**
     * Retourne les pays favoris de l'utilisateur
     *
     * @return void
     */
    public function favoris()
    {
       return $this->hasMany(Favoris::class)
                   ->join('pays','pays.id', 'pays_id');
    }

    /**
     * Vérifie si l'utilisateur est admin
     *
     * @return boolean
     */
    public function isAdmin()
    {
        if (Auth::user()->role_id == 1) {
            return true;
        }
        else {
            return false;
        }
    }

    /**
     * Vérifie si l'utilisateur est vérifié
     *
     * @return boolean
     */
    public function isVerified()
    {
        if (Auth::user()->email_verified_at != null) {
            return true;
        }
        else {
            return false;
        }
    }

}
