<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Annonces extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'titre',
        'description',
        'date',
        'duree'
    ];

    /**
     * Retourne le pays concernée par l'annonce
     *
     * @return void
     */
    public function pays()
    {
        return $this->belongsTo(Pays::class, 'pays_id');
    }

    /**
     * Retourne l'utilisateur qui crée l'annonce
     *
     * @return void
     */
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    /**
     * Récupère les commentaires de l'annonce concerné avec les utilisateurs
     *
     * @return void
     */
    public function commentaires()
    {
        return $this->hasMany(Commentaires::class, 'element_id')
                    ->where('type', 'App\Annonces')
                    ->join('users', 'users.id', 'user_id')
                    ->select('users.pseudo','users.color', 'users.avatar', 'commentaires.*');
    }

}
