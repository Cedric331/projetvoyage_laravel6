<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Periodes extends Model
{
    protected $fillable = [
        'saison_id', 'mois_id', 'pays_id'
    ];

    /**
     * Retourne le pays de la période
     *
     * @return void
     */
    public function pays()
    {
        return $this->belongsTo(Pays::class);
    }
}
