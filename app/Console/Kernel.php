<?php

namespace App\Console;

use App\User;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // Supprime les utilisateurs qui n'ont pas validé le mail 72h après l'inscription
        $schedule->call(function () {
            $jours = "+72 hours";
            $users = User::where('email_verified_at', null)->get();

            foreach ($users as $user) {
                $date = (strtotime($user->created_at ) + strtotime($jours));
                if (time() > $date) {
                    $user->delete();
                }
            }
        });
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
