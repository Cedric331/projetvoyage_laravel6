<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Saisons extends Model
{
    protected $fillable = [
        'saison'
    ];
}
