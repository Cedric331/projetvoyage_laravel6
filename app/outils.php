<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

// Regroupement des fonctions

// creation du lien vers la page compte perso de l'utilisateur ou celui d'un autre compte utilisateur
function lienCompte($variable, $classe = null)
{

    if (Auth::check() && $variable->pseudo == Auth::user()->pseudo)
    {
        $lien = "<a href=\"/compte\" class=\"text-dark $classe\">";
    }
    else
    {
        $lien = "<a href=\"/voir-compte/$variable->pseudo\" class=\"text-dark $classe\">";
    }
    return $lien;
}

// Affiche l'avatar de l'utilisateur avec vérification si l'utilisateur à téléchargé un avatar ou si il utilise celui par défaut
function verificationAvatar($variable)
{
    $lien = lienCompte($variable);

    $oldfileexists = Storage::disk('public')->exists($variable->avatar);


        if($oldfileexists)
        {
            return $lien.'<img src="/storage/'.$variable->avatar.'" alt="avatar du profil" class="d-flex mr-3 avatar"></a>';
        }

        else
        {
            return $lien.'<img class="d-flex mr-3"
                    src="https://eu.ui-avatars.com/api/?size=40&background='.$variable->color.'&rounded=true&name='.$variable->pseudo.'"
                    alt="avatar du profil"></a>';
        }
}

function avatarUser($variable)
{
    $oldfileexists = Storage::disk('public')->exists($variable->avatar);

        if($oldfileexists)
        {
            return '<img src="/storage/'.$variable->avatar.'" alt="avatar du profil" style="width:100px;height:100px;" class="z-depth-1 rounded-circle img-fluid">';
        }
        else
        {
            return '<img class="z-depth-1 rounded-circle img-fluid" style="width:100px;"
                    src="https://eu.ui-avatars.com/api/?size=40&background='.$variable->color.'&rounded=true&name='.$variable->pseudo.'"
                    alt="avatar du profil">';
        }
}

/**
 * Interroge l'api restCountry pour récupérer des informations sur les pays avec leurs code ISO 3166-1
 * https://restcountries.eu/#api-endpoints-name
 * @param [type] $iso contient le code ISO du pays
 * @return void
 */
function restCountry($iso)
{
    $client = new GuzzleHttp\Client();
    $res = $client->get('https://restcountries.eu/rest/v2/alpha/'.$iso.'?fields=flag;capital');
    $info = $res->getBody();
    $info = json_decode($info);
    return $info;
}

/**
 * Interroge l'api restCountry pour récupérer la population du Pays
 * https://restcountries.eu/
 * @param [type] $iso contient le code ISO du pays
 * @return void
 */
function restCountryPop($iso)
{
    $client = new GuzzleHttp\Client();
    $res = $client->get('https://restcountries.eu/rest/v2/alpha/'.$iso.'?fields=population');
    $pop = $res->getBody();
    $pop = json_decode($pop);
    $pop = intval($pop->population);

    // Permet de mettre des espaces dans le nombre
    // https://www.php.net/manual/fr/function.number-format.php
    $population = number_format($pop, 0, ' ', ' ');

    return $population .' habitants';
}
