<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Amis extends Model
{
    protected $fillable = [
        'amis',
        'demande',
        'user_id'
    ];

    /**
     * Retourne l'utilisateur qui reçoit la demande d'amis
     *
     * @return void
     */
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}
