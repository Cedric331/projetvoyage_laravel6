<?php

namespace App\Notifications;

use App\Pays;
use App\User;
use App\Annonces;
use App\Commentaires;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class CommentaireLike extends Notification
{
    use Queueable;


    protected $commentaire;
    protected $element_id;
    protected $type;
    protected $user;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(Commentaires $commentaire, $element_id, $type, User $user)
    {
            $this->commentaire = $commentaire;
            $this->element_id = $element_id;
            $this->type = $type;
            $this->user = $user;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database'];
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
            return [
                'element' => $this->element_id,
                'type' => $this->type,
                'CommentId' => $this->commentaire->id,
                'userName' => $this->user->pseudo
            ];
    }
}
