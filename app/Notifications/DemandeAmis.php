<?php

namespace App\Notifications;

use App\Amis;
use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class DemandeAmis extends Notification
{
    use Queueable;

    protected $demandeAmis;
    protected $user;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(Amis $demandeAmis, User $user)
    {
        $this->demandeAmis = $demandeAmis;
        $this->user = $user;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database'];
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'AmisId' => $this->demandeAmis->id,
            'userName' => $this->user->pseudo
        ];
    }
}
