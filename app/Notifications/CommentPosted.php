<?php

namespace App\Notifications;

use App\User;
use App\Annonces;
use Illuminate\Bus\Queueable;
use Illuminate\Support\Facades\Auth;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class CommentPosted extends Notification implements ShouldQueue
{
    use Queueable;

    protected $annonce;
    protected $user;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(Annonces $annonce, User $user)
    {
            $this->annonce = $annonce;
            $this->user = $user;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database'];
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'annonceTitle' => $this->annonce->titre,
            'annonceId' => $this->annonce->id,
            'userName' => $this->user->pseudo
        ];
    }
}
