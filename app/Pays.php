<?php

namespace App;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

class Pays extends Model
{
        /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nom_pays',
        'prix',
        'iso',
        'image_Principal',
        'description_pays',
        'description_climat',
        'description_partir',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
    'created_at', 'update_at',
    ];

    /**
     * Récupère les commentaires du Pays concerné avec les utilisateurs
     *
     * @return void
     */
    public function commentaires()
    {
        return $this->hasMany(Commentaires::class, 'element_id')
                ->where('type', 'App\Pays')
                ->join('users', 'users.id', 'user_id')
                ->select('users.pseudo', 'users.color', 'users.avatar', 'commentaires.*');
    }

    /**
     * Récupère les lieux touristiques du Pays
     *
     * @return void
     */
    public function tourismes()
    {
        return $this->hasMany(Tourismes::class);
    }

    /**
     * Récupère le continent du Pays
     *
     * @return void
     */
    public function continent()
    {
        return $this->belongsTo(Continents::class);
    }

    /**
     * Récupère les expériences du Pays
     *
     * @return void
     */
    public function experience()
    {
        return $this->hasMany(Experience::class)
            ->join('users', 'users.id', '=', 'experiences.user_id')
            ->select('experiences.id', 'titre_experience', 'description_experience', 'note_experience', 'pseudo', 'color', 'avatar', 'experiences.user_id');
    }

}
