<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Continents extends Model
{
   protected $fillable = [
      'nom_continent',
  ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at', 'update_at',
        ];

}
