<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Experience extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'titre_experience',
        'description_experience',
        'note_experience',
    ];

    /**
     * Retourne l'utilisateur qui a crée l'expérience
     *
     * @return void
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
