<?php

namespace App\Http\Controllers;

use App\Amis;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Routing\Route;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Illuminate\Notifications\DatabaseNotification;


class CompteController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Retourne la vue du compte de l'utilisateur
     *
     * @return void
     */
    public function index()
    {
            $user = User::findOrFail(Auth::user()->id);
            $experiences = $user->experience;
            $favoris = $user->favoris;

            return view('auth.compte', ['experiences' => $experiences, 'favoris' => $favoris]);
    }

    /**
     * Suppression de l'utilisateur avec message de confirmation sur la page d'accueil
     * Vérifie également si un dossier concernant l'utilisateur existe et le supprime
     * @return void
     */
    public function destroy()
    {
        $user = User::findOrFail(Auth::user()->id);

        // Vérifie si un dossier concernant l'utilisateur existe et le supprime si c'est le cas
        $exists = Storage::disk('public')->exists('avatars/'.$user->id);
        if ($exists) {
            Storage::disk('public')->deleteDirectory('avatars/'.$user->id);
        }

        // Supprime de la DB les amis de cette utilisateur
        $amis = Amis::where('amis', $user->id)->get();
        foreach ($amis as $value) {
           $value->delete();
        }

        // Supprime les notifications de l'utilisateur
        $notifications = DatabaseNotification::where('notifiable_id', Auth::user()->id)->get();
        foreach ($notifications as $notification) {
            $notification->delete();
        }

        $user->delete();

        notify()->info('Votre compte a bien été supprimé !');
        return redirect('accueil');
    }

   /**
    * Upload de l'avatar de l'utilisateur avec contrôle de l'avatar envoyé par l'utilisateur
    * et supprime l'avatar précédant si celui-ci existe
    * $request contient l'avatar uploadé de l'utilisateur
    * @param Request $request
    * @return void
    */
    public function avatarUpdate(Request $request)
    {
        $request->validate([
            'avatar' => 'required|image|mimes:jpeg,png,jpg|max:2048'
        ]);

        $user = User::findOrFail(Auth::user()->id);

        // Vérification si un avatar existe déjà et le supprime si c'est le cas
        $exists = Storage::disk('public')->exists('avatars/'.$user->id);
        if ($exists) {
            Storage::disk('public')->deleteDirectory('avatars/'.$user->id);
        }

        $path = $request->file('avatar')->store('avatars/'.$user->id, 'public');
        $user->avatar = $path;
        $user->save();

        notify()->success('Avatar à jour');
        return redirect()->back();
    }

    /**
     * Modifie la couleur de l'avatar par défaut
     * $request contient la couleur sélectionné par l'utilisateur
     *
     * @param Request $request
     * @return void
     */
    public function avatarColor(Request $request)
    {
        $request->validate([
            'color' => 'required','alpha_dash','string'
        ]);

        switch ($request->color) {
            case 'rouge':
                $request->color = 'd3332d';
                break;
            case 'bleu':
                $request->color = '2d9ed3';
                break;
            case 'vert':
                $request->color = '288d39';
                break;
            case 'jaune':
                $request->color = 'b3c03e';
                break;
            default:
                $request->color = '84b2cf';
                break;
        }

        $user = User::findOrFail(Auth::user()->id);
        $user->color = $request->color;
        $user->save();

        notify()->success('Avatar à jour');
        return redirect()->back();
    }

    /**
     * Supprime l'avatar du compte
     *
     * @return void
     */
    public function avatarDelete()
    {
        $user = User::findOrFail(Auth::user()->id);

        // Vérification si un avatar existe et le supprime si c'est le cas
        $exists = Storage::disk('public')->exists('avatars/'.$user->id);
        if ($exists) {
            Storage::disk('public')->deleteDirectory('avatars/'.$user->id);
            notify()->info('Avatar supprimé');
            return redirect()->back();
        }

        notify()->info('Erreur lors de la suppression de l\'avatar');
        return redirect()->back();
    }

    /**
     * Retourne le formulaire de mise à jour de l'adresse email
     *
     * @return void
     */
    public function emailUpdateForm()
    {
        return view('auth.compte-email');
    }

    /**
     * Mise à jour de l'adresse email de l'utilisateur après vérification des éléments
     *
     * @param Request $request
     * @return void
     */
    public function emailUpdate(Request $request)
    {

        $request->validate([
        'emailUpdate' => ['required', 'string', 'email', 'max:100', 'unique:users,email']
        ]);

        $user = User::findOrFail(Auth::user()->id);
        $user->email = $request->emailUpdate;
        $user->save();

        notify()->info('Adresse email modifiée!');
        return redirect('compte');
    }

    /**
     * Retourne la view pour éditer le pseudo
     *
     * @return void
     */
    public function pseudo()
    {
        return view('auth.compte-pseudo');
    }

    /**
     * Permet de modifier le pseudo de l'utilisateur
     * $request contient le pseudo que l'utilisateur à tapé
     *
     * @param Request $request
     * @return void
     */
    public function pseudoUpdate(Request $request)
    {
        $request->validate([
            'pseudo' => ['required', 'alpha_dash', 'string','min:4', 'max:15','unique:users,pseudo'],
        ]);

        $user = User::findOrFail(Auth::user()->id);
        $user->pseudo = $request->pseudo;
        $user->save();

        notify()->success('Pseudo modifiée!');
        return redirect('compte');
    }

    /**
     * Retourne les expériences de l'utilisateur
     *
     * @return void
     */
    public function mesExperiences()
    {
        $user = User::findOrFail(Auth::user()->id);

        $experiences = $user->experience;

        return view('auth.mes-experience', ['experiences' => $experiences]);
    }

    /**
     * Marque toutes les notifications de l'utilisateur comme lue
     *
     * @return void
     */
    public function notificationRead()
    {
        $user = User::find(Auth::user()->id);
        $user->unreadNotifications()->update(['read_at' => now()]);

        return redirect()->back()->withInput();
    }

    public function indexNotification()
    {
         return view('auth.notification');
    }

}
