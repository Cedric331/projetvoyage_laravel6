<?php

namespace App\Http\Controllers;

use notify;
use App\Amis;
use App\User;
use Illuminate\Http\Request;
use App\Notifications\DemandeAmis;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Notifications\DatabaseNotification;


class AmisController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Récupère en paramètre le pseudo de l'utilisateur sélectionné
     * Sélectionne l'utilisateur dans la DB passé en paramètre et l'affiche sur la page voir-compte
     * Vérifie si une demande d'amis à déjà été envoyée
     * @param [type] $pseudo
     * @return void
     */
    public function voirCompte($pseudo)
    {
        // Récupération de l'utilisateur sélectionné avec le pseudo en paramètre
        $user = User::where('pseudo', $pseudo)->firstOrFail();
        // Récupération des informations de l'utilisateur authentifié
        $userAuth = User::findOrFail(Auth::user()->id);

        if ($user == Auth::user()) {
            return redirect()->route('compte');
        }

        // Vérification si une demande d'amis est en cours
        // Retourne les demandes d'amis en cours entre les deux utilisateurs
        $demandeEnCours1 = $user->verificationDemandeAmis($userAuth->id);
        $demandeEnCours2 = $userAuth->verificationDemandeAmis($user->id);

        // Si les vérifications ont une valeur différente de 0 c'est qu'une demande d'amis est en cours
            if ($demandeEnCours1->count() != 0 || $demandeEnCours2->count() != 0)
            {
                $demande = true;
            }
            else
            {
                $demande = false;
            }

        // Vérification si les utilisteurs sont déjà amis
        $verificationAmis1 = $user->verificationAmis($userAuth->id);
        $verificationAmis2 = $userAuth->verificationAmis($user->id);

            // Si les vérifications un objet non vide alors les utilisateurs sont amis
            if ($verificationAmis1->count() != 0 || $verificationAmis2->count() != 0)
            {
                $amis = true;
            }
            else
            {
                $amis = false;
            }

            $experience = $user->experience;
            $favoris = $user->favoris;

        return view('amis/voir-compte', [
        'user' => $user,
        'favoris' => $favoris,
        'experiences' =>$experience,
        'demandeEnCours' => $demande,
        'amis' => $amis]);
    }

    /**
     * Retourne la page Amis avec les demandes en cours et les amis du compte de l'utilisateur
     * ($demandes) retourne les invitations d'ajout à la liste d'amis
     * ($amis) retourne les amis de l'utilisateur
     *
     * @return void
     */
    public function indexAmis()
    {
        $user = User::findOrFail(Auth::user()->id);

        // Récupère les demandes d'amis en cours de l'utilisateur
        $demandes = $user->demandeAmis;

        // Récupère les amis de l'utilisateur
        $amis = $user->amis;

        return view('auth/compte-amis', ['demandes' => $demandes, 'mesAmis' => $amis]);
    }


    public function indexAmisNotification($notification)
    {
        $user = User::findOrFail(Auth::user()->id);
        $notifi = DatabaseNotification::find($notification);
        $notifi->markAsRead();

        // Récupère les demandes d'amis en cours de l'utilisateur
        $demandes = $user->demandeAmis;

        // Récupère les amis de l'utilisateur
        $amis = $user->amis;

        return view('auth/compte-amis', ['demandes' => $demandes, 'mesAmis' => $amis]);
    }

    /**
     * Permet d'effectuer une demande d'amis
     *
     * @param [type] $id contient l'id de l'utilisateur qui réceptionne la demande
     * @return void
     */
    public function ajouterAmis($id)
    {
        $user = User::findOrFail($id);

        $amis = Amis::firstOrCreate(
         ['amis' => Auth::user()->id, 'user_id' => $user->id],
        );

        $amis->user->notify(new DemandeAmis($amis, Auth::user()));

        notify()->info('Demande d\'amis envoyée');
        return redirect()->back();
    }

    /**
     * Supprime la demande d'amis et la notification en lien si non lu
     * @param [type] $id contient l'id de l'utilisateur qui a reçu la demande d'amis
     * @return void
     */
    public function deleteDemande($id)
    {
      $user = User::findOrFail($id);
      $auth = Auth::user();

      // Récupère la demande d'amis envoyée
      $amis = Amis::where('amis', $auth->id)
                  ->where('user_id', $user->id)
                  ->where('demande', false)
                  ->firstOrFail();

      // Récupère la notification en lien avec la demande d'amis
      $notification = DatabaseNotification::where('type', 'App\Notifications\DemandeAmis')
                  ->where('data', '{"AmisId":'.$amis->id.',"userName":"'.$auth->pseudo.'"}')
                  ->where('read_at', null)->first();

      // Vérifie si la notification existe et n'est pas null
      // Puis la supprime si true

      if ($notification){
         $notification->delete();
      }

      $amis->delete();

      notify()->info('Demande d\'amis supprimée');
      return redirect()->back();
    }

    /**
     * la ligne de la table "DemandeAmis" concernée est supprimé de la DB
     *
     * @param [type] $id correspond à l'id de la demande d'amis
     * @return void
     */
    public function refuser($id)
    {
        $demande = Amis::findOrFail($id);
        $demande->delete();

        notify()->info('Demande d\'amis refusée');
        return redirect()->back();
    }

    /**
     * Je récupère la demande concernée grâce à son id dans la DB et je modifie "demande" sur true
     * Ensuite je créé une nouvelle demande pour indiquer que les utilisateurs sont amis
     *
     * @param [type] $id contient l'id de la demande d'amis
     * @return void
     */
    public function accepter($idDemande)
    {
       // Update de la demande
         $demande = Amis::findOrFail($idDemande);
         $demande->demande = true;
         $demande->save();

        // Création d'une ligne dans la DB pour indiquer les utilisateur qui sont amis
        $amis = Amis::firstOrCreate(
         ['amis' => $demande->user_id, 'user_id' => $demande->amis],
        );
        $amis->demande = true;
        $amis->save();

        notify()->success("Demande d'amis acceptée");
        return redirect()->back();
    }

    /**
     * Récupère dans la table amis les lignes amis des utilisateurs concernées
     * et les supprimes
     * @param [type] $pseudo
     * @return void
     */
    public function supprimerAmis($pseudo)
    {
       $userAuth =  User::findOrFail(Auth::user()->id);
       $userAmis = User::where('pseudo', $pseudo)->firstOrFail();

       $amis = Amis::where([['user_id', $userAmis->id], ['amis', $userAuth->id]]);
       $amis->delete();

       $amis = Amis::where([['amis', $userAmis->id], ['user_id', $userAuth->id]]);
       $amis->delete();

       notify()->info('Amis supprimé');
       return redirect()->back();
    }
}
