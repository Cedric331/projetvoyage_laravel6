<?php

namespace App\Http\Controllers;

use App\Pays;
use App\Experience;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Validator;
use GrahamCampbell\Security\Facades\Security;

class ExperienceController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Vérification des informations et enregistrement de l'élément
     *
     * @param Request $request
     * @param [type] $nomPays
     * @return void
     */
    public function store(Request $request, $nomPays)
    {
        // Nettoyage contre les failles XSS
        if ($request->titre != null){
            $request->titre = Security::clean($request->titre);
        }
        if ($request->description != null){
            $request->description = Security::clean($request->description);
        }
        if ($request->note != null){
            $request->note = Security::clean($request->note);
        }

        $pays = Pays::where('nom_pays', $nomPays)->firstOrFail();

        $validator  = Validator::make($request->all(), [
            'titre' => 'required|min:4|max:30',
            'description' => 'required|min:10|max:255',
            'note' => 'required|numeric|min:0|max:5'
        ]);
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }


        $experience = new Experience;
        $experience->titre_experience = $request->titre;
        $experience->description_experience = $request->description;
        $experience->note_experience = $request->note;
        $experience->user_id = Auth::user()->id;
        $experience->pays_id = $pays->id;
        $experience->save();

        notify()->info("Expérience de voyage ajoutée");

        return redirect()->route('pays.pays',[$pays->nom_pays]);
    }

    /**
     * Affichage du formulaire pour poster une expérience avec le pays ciblé
     *
     * @param [type] $pays
     * @return void
     */
    public function form($pays)
    {
        $pays = Pays::where('nom_pays', $pays)->firstOrFail();

        return view('pays.partage', ['pays' => $pays]);
    }

    /**
     * Affichage du formulaire pour modifier une expérience
     *
     * @param [type] $nomPays
     * @param [type] $idExperience
     * @return void
     */
    public function getForm($idExperience)
    {
        $idExperience =  Crypt::decrypt($idExperience);
        $experience = Experience::findOrFail($idExperience);

        if (Auth::user()->id != $experience->user_id) {
            abort(403);
        }

        return view('pays.experience', ['experience' => $experience]);
    }

    /**
     * Mise à jour de l'expérience de l'utilisateur
     * Récupère la requête et l'id de l'expérience avec le pays rattaché
     * Effectue une validation des éléments
     * Mise à jour de l'expérience et renvoi sur la page du pays concerné
     * @param Request $request
     * @param [type] $idExperience contient l'id de l'expérience
     * @return void
     */
    public function update(Request $request, $idExperience)
    {
        $experience = Experience::findOrFail($idExperience);

        if (Auth::user()->id != $experience->user_id) {
            abort(403);
        }

        // Protection contre les failles XSS
        if ($request->titre != null){
            $request->titre = Security::clean($request->titre);
        }
        if ($request->description != null){
            $request->description = Security::clean($request->description);
        }
        if ($request->note != null){
            $request->note = Security::clean($request->note);
        }

        $validator  = Validator::make($request->all(), [
            'titre' => 'required|min:4|max:30',
            'description' => 'required|min:10|max:255',
            'note' => 'required|numeric|min:0|max:5'
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator);
        }

        $experience->titre_experience = $request->titre;
        $experience->description_experience = $request->description;
        $experience->note_experience = $request->note;
        $experience->save();

        $pays = Pays::findOrFail($experience->pays_id);

        notify()->info("Expérience de voyage modifiée");

        return redirect()->route('pays.pays',[$pays->nom_pays]);
    }

    /**
     * Permet de supprimer l'expérience de l'utilisateur
     * récupère l'id de l'expérience et le pays rattaché
     * supprime et renvoi l'utilisateur sur la page du pays concerné
     * @param [type] $idExperience
     * @return void
     */
    public function delete($idExperience)
    {
        $experience = Experience::findOrFail($idExperience);

        if (Auth::user()->id != $experience->user_id) {
            abort(403);
        }

        $pays = Pays::findOrFail($experience->pays_id);
        $experience->delete();

        notify()->info("Expérience de voyage supprimée");

        return redirect()->route('pays.pays',[$pays->nom_pays]);
    }

}
