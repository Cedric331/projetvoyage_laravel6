<?php

namespace App\Http\Controllers;

use App\Mois;
use App\Pays;
use App\Saisons;
use App\Periodes;
use App\Continents;
use Illuminate\Http\Request;
use Illuminate\Pagination\Paginator;
use Illuminate\Pagination\LengthAwarePaginator;


class AccueilController extends Controller
{
    /**
     * Récupère les paramètres envoyé par l'utilisateur et les retournes sur la page de recherche pour le component Livewire
     * $request contient les critères de recherche de l'utilisateur
     * @param Request $request
     */
    public function searchPays(Request $request)
    {
        $request->validate([
            'budget' => 'required',
            'saison' => 'required_with_all:mois',
            'mois' => 'required_with_all:saison'
        ], [
            'saison.required_with_all' => 'Le champs est requis lorque vous sélectionnez un mois dans la liste.',
            'mois.required_with_all' => 'Le champs est requis lorque vous sélectionnez une saison dans la liste.'
        ]);

        return view('pays.search', [
            'budget' => $request->budget,
            'continent' => $request->continent,
            'saison' => $request->saison,
            'mois' => $request->mois]);
    }
}
