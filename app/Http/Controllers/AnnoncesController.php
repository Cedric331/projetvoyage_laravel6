<?php

namespace App\Http\Controllers;

use App\Pays;
use App\User;
use App\Annonces;
use App\Commentaires;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Crypt;
use GrahamCampbell\Security\Facades\Security;
use Illuminate\Notifications\DatabaseNotification;

class AnnoncesController extends Controller
{
    /**
     * Retourne la page des annonces avec la liste des annonces en ligne
     *
     * @return void
     */
    public function annonceIndex()
    {
        $annonces = Annonces::orderBy('created_at', 'DESC')
                            ->join('users', 'users.id', 'user_id')
                            ->join('pays', 'pays.id', 'pays_id')
                            ->select('pseudo', 'nom_pays', 'image_principal', 'annonces.*')
                            ->paginate(10);

        return view('annonce.annonce', ['annonces' => $annonces]);
    }


    /**
     * Récupère la vue de l'annonce avec le pays concerné et l'utilisateur qui a créer l'annonce
     *
     * @param [type] $idAnnonce
     * @return void
     */
    public function annonceView($idAnnonce)
    {
        $idAnnonce = Crypt::decrypt($idAnnonce);
        $annonce = Annonces::findOrFail($idAnnonce);
        $pays = $annonce->pays;
        $user = $annonce->user;

        return view('annonce.annonceview', ['annonce' => $annonce, 'pays' => $pays, 'user' => $user]);
    }

    /**
     * Récupère la vue de l'annonce avec le pays concerné et l'utilisateur qui a créer l'annonce
     * Marque la notification comme lue
     * @param Annonces $annonce
     * @param DatabaseNotification $notification
     * @return void
     */
    public function annonceViewNotification($annonce, $notification)
    {
        $notify = DatabaseNotification::find($notification);
        if ($notify) {
            $notify->markAsRead();
        }

        $annonce = Annonces::findOrFail($annonce);
        $pays = $annonce->pays;
        $user = $annonce->user;

        return view('annonce.annonceview', ['annonce' => $annonce, 'pays' => $pays, 'user' => $user]);
    }

    /**
     * Affiche le formulaire pour la création d'une annonce
     *
     * @return void
     */
    public function annonceFormulaire()
    {
        $pays = Pays::All();

        return view('annonce.annonceformulaire', ['listes' =>$pays]);
    }

    /**
     * Vérifie les informations et l'enregistre dans la DB
     *
     * @param Request $request
     * @return void
     */
    public function annoncePost(Request $request)
    {
        // Nettoyage contre les failles XSS
        if ($request->titre != null){
            $request->titre = Security::clean($request->titre);
        }
            if ($request->description != null){
            $request->description = Security::clean($request->description);
        }
            if ($request->pays != null){
            $request->pays = Security::clean($request->pays);
        }
            if ($request->date != null){
            $request->date = Security::clean($request->date);
        }
            if ($request->duree != null) {
            $request->duree = Security::clean($request->duree);
        }

        $request->validate([
            'titre' => 'required|string|max:50',
            'description' => 'required|string|max:500|min:20',
            'date' => 'date|nullable|after:today',
            'duree' => 'string|nullable|max:50',
            'pays' => 'required'
        ]);

        $pays = Pays::findOrFail($request->pays);

        $annonce = new Annonces;
        $annonce->titre = $request->titre;
        $annonce->date = $request->date;
        $annonce->pays_id = $pays->id;
        $annonce->duree = $request->duree;
        $annonce->user_id = Auth::user()->id;
        $annonce->description = $request->description;
        $annonce->save();

        notify()->success("Votre annonce est en ligne");
        return redirect()->route('annonce');
    }

    /**
     * Récupère l'annonce et la supprime
     *
     * @param [type] $idAnnonce contient l'id de l'annonce
     * @return void
     */
    public function Annoncedelete($idAnnonce)
    {
        $annonce = Annonces::findOrFail($idAnnonce);

        if (Auth::user()->id != $annonce->user_id) {
            abort(403);
        }

        $annonce->delete();

        notify()->info("Votre annonce est supprimée");
        return redirect()->route('annonce');
    }

    /**
     * Affiche le formulaire pour la modification de l'annonce et vérifie que l'utilisateur
     * est bien le créateur de l'annonce
     *
     * @param Request $request
     * @return void
     */
    public function annonceUpdate($idAnnonce)
    {
        $idAnnonce = Crypt::decrypt($idAnnonce);
        $annonce = Annonces::findOrFail($idAnnonce);
        $listes = Pays::All();
        $pays = Pays::find($annonce->pays_id);

        if (Auth::user()->id != $annonce->user_id) {
            abort(403);
        }

        return view('annonce.annonceupdate', ['annonce' => $annonce, 'pays' => $pays, 'listes' => $listes]);
    }

    /**
     * Vérification des informations et modification de l'annonce
     *
     * @param Request $request
     * @param [type] $idAnnonce contient l'id de l'annonce à modifier
     * @return void
     */
    public function annoncePatch(Request $request, $idAnnonce)
    {
        $idAnnonce = Crypt::decrypt($idAnnonce);
        $annonce = Annonces::find($idAnnonce);

        if (Auth::user()->id != $annonce->user_id) {
            abort(403);
        }

        // Nettoyage contre les failles XSS
        if ($request->titre != null){
            $request->titre = Security::clean($request->titre);
        }
        if ($request->description != null){
            $request->description = Security::clean($request->description);
        }
        if ($request->pays != null){
            $request->pays = Security::clean($request->pays);
        }
        if ($request->date != null){
            $request->date = Security::clean($request->date);
        }
        if ($request->duree != null) {
            $request->duree = Security::clean($request->duree);
        }

        $request->validate([
            'titre' => 'required|string|max:50',
            'description' => 'required|string|max:500|min:20',
            'date' => 'date|nullable|after:today',
            'duree' => 'string|nullable|max:50',
            'pays' => 'required'
        ]);

        $pays = Pays::findOrFail($request->pays);

        $annonce->titre = $request->titre;
        $annonce->date = $request->date;
        $annonce->pays_id = $pays->id;
        $annonce->duree = $request->duree;
        $annonce->user_id = Auth::user()->id;
        $annonce->description = $request->description;
        $annonce->save();

        notify()->info("Votre annonce est mise à jour");
        return redirect()->route('annonce');
    }

    public function annonceNotification($notification)
    {
        $notify = DatabaseNotification::find($notification);
        if ($notify) {
            $notify->markAsRead();
        }

        $commentaire = Commentaires::findOrFail($notify->commentaire_id);

        $annonce = Annonces::findOrFail($commentaire->annonce_id);
        $pays = $annonce->pays;
        $user = $annonce->user;

        return view('annonce.annonceview', ['annonce' => $annonce, 'pays' => $pays, 'user' => $user]);
    }
}
