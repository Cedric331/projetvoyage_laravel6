<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Mail\Contact;
use Illuminate\Support\Facades\Mail;

class ContactController extends Controller
{

    /**
     * Validation de la requete puis appel de Mail.Contact pour la création du mail et son envoie
     * puis retour sur la page d'accueil avec confirmation
     *
     * @param Request $request
     * @return void
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|string|max:30',
            'email' => 'required|email',
            'sujet' => 'required|max:30|string',
            'message' => 'required|string|max:500'
        ]);

        Mail::to(env('MAIL_USERNAME') )
            ->queue(new Contact($request->except('_token')));

        notify()->info("Message envoyée");
        return redirect()->route('accueil');
    }

}
