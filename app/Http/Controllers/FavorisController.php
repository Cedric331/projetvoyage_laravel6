<?php

namespace App\Http\Controllers;

use App\Pays;
use App\Favoris;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class FavorisController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Ajoute un Pays dans la liste des favoris de l'utilisateur
     *
     * @param Pays $pays contient le nom du pays
     * @return void
     */
    public function addFavoris($pays)
    {
        $pays = Pays::where('nom_pays', $pays)->firstOrFail();

        $favoris = new Favoris;
        $favoris->pays_id = $pays->id;
        $favoris->user_id = Auth::user()->id;
        $favoris->save();

        notify()->success("Pays ajouté dans les favoris");
        return redirect()->back();
    }

    /**
     * Supprime un pays de la liste des favoris
     *
     * @param [type] $pays
     * @return void
     */
    public function deleteFavoris($pays)
    {
        $favoris = Favoris::where('pays_id', $pays)
                          ->where('user_id', Auth::user()->id)
                          ->first();

        $favoris->delete();

        notify()->info("Pays supprimé des favoris");
        return redirect()->back();
    }
}
