<?php

namespace App\Http\Controllers;

use App\Pays;
use Illuminate\Http\Request;

class SearchController extends Controller
{
    /**
     * Fonction recherche de pays
     */
    public function search(Request $recherche)
    {
        request()->validate([
            'recherche' => 'required'
        ]);

        $recherche = request()->input('recherche');

        $pays = Pays::orderBy('nom_pays')
                    ->where('nom_pays', 'like',"$recherche%")
                    ->paginate(9);

        return view('pays.liste', ['listes' => $pays]);
    }
}
