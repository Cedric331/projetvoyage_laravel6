<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ComparateurController extends Controller
{
    public function indexComparateur()
    {
       return view('comparateur');
    }

    public function indexAirbnb()
    {
       return view('airbnb');
    }
}
