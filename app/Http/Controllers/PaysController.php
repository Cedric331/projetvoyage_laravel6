<?php

namespace App\Http\Controllers;

use App\Pays;
use App\User;
use App\Continents;
use App\CommentairePays;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Notifications\DatabaseNotification;

class PaysController extends Controller
{

    /**
     * Affiche la liste des Pays par ordre croissant et avec une pagination de 9 éléments
     *
     * @return void
     */
    public function liste()
    {
        $listes = Pays::orderBy('nom_pays')->paginate(9);

        return view('pays.liste', ['listes' => $listes]);
    }

    /**
     * Retourne la page pays sélectionné ainsi que les tables qui ont un lien avec ce Pays
     *
     * @param [type] $nomPays contient le nom du Pays
     * @return void
     */
    public function affichePays($nomPays)
    {
        $pays = Pays::where('nom_pays', $nomPays)->firstOrFail();

        $continent = $pays->continent;
        $tourismes = $pays->tourismes;
        $experiences = $pays->experience;

        return view('pays.pays', ['pays' => $pays, 'continent' => $continent, 'tourismes' => $tourismes, 'experiences' => $experiences]);
    }

    /**
     * Retourne la page du pays depuis la notification like du commentaire
     *
     * @param [type] $nomPays contient le nom du Pays
     * @param [type] $notification contient l'id de la notification
     * @return void
     */
    public function paysNotification($id, $notification)
    {

        $pays = Pays::findOrFail($id);

        // Marque la notification comme lue
        $notify = DatabaseNotification::find($notification);
        if ($notify) {
            $notify->markAsRead();
        }

        $continent = $pays->continent;
        $tourismes = $pays->tourismes;
        $experiences = $pays->experience;

        return view('pays.pays', ['pays' => $pays, 'continent' => $continent, 'tourismes' => $tourismes, 'experiences' => $experiences]);
    }
}
