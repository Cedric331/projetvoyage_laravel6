<?php

namespace App\Http\Controllers;

use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Cmgmyr\Messenger\Models\Thread;
use Cmgmyr\Messenger\Models\Message;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Session;
use Cmgmyr\Messenger\Models\Participant;
use GrahamCampbell\Security\Facades\Security;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class MessagesController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Show all of the message threads to the user.
     *
     * @return mixed
     */
    public function index()
    {
        // All threads, ignore deleted/archived participants
        // $threads = Thread::getAllLatest()->get();

        // All threads that user is participating in
        $threads = Thread::forUser(Auth::id())->latest('updated_at')->get();

        // All threads that user is participating in, with new messages
        // $threads = Thread::forUserWithNewMessages(Auth::id())->latest('updated_at')->get();

        return view('messenger.index', compact('threads'));
    }

    /**
     * Shows a message thread.
     *
     * @param $id
     * @return mixed
     */
    public function show($id)
    {
        $id = Crypt::decrypt($id);
        try {
            $thread = Thread::findOrFail($id);
        } catch (ModelNotFoundException $e) {
            Session::flash('error_message', 'Cette discussion n\'existe pas.');

            return redirect()->route('messages');
        }

        $user = User::findOrFail(Auth::user()->id);

        // Vérifie si l'utilisateur est dans la conversation sinon le renvoie sur la page messagerie
        if ($thread->hasParticipant($user->id) == false) {
            Session::flash('error_message', 'Cette discussion n\'existe pas.');
            return redirect()->route('messages');
        }

        // Récupère la liste d'amis de l'utilisateur qui ne sont pas dans la conversation
        $users = $user->amis->whereNotIn('amis', $thread->participantsUserIds());
        // Récupère le pseudo des utilisateurs qui sont dans la conversation
        $participants = $thread->participantsString(Auth::id());

        // Indique pour l'utilisateur destinataire qu'il a lu le message
        $participant = Participant::firstOrCreate([
            'thread_id' => $thread->id,
            'user_id' => Auth::id(),
        ]);
        $participant->last_read = new Carbon;
        $participant->save();

        return view('messenger.show', compact('thread', 'users', 'participants'));
    }

    /**
     * Créer une nouvelle conversation
     *
     * @return mixed
     */
    public function create()
    {
        $user = User::findOrFail(Auth::user()->id);

        $users = $user->amis;

        return view('messenger.create', compact('users'));
    }

    /**
     * Stores a new message thread.
     *
     * @return mixed
     */
    public function store(Request $request)
    {
        // Protection contre les failles XSS
        if ($request->message != null && $request->subject != null) {
            $request->message = Security::clean($request->message);
            $request->subject = Security::clean($request->subject);
        }


        $request->validate([
            'subject' => 'required|min:3|max:50',
            'message' => 'required|max:255'
        ]);

        $thread = Thread::create([
            'subject' => $request->subject,
        ]);

        // Message
        Message::create([
            'thread_id' => $thread->id,
            'user_id' => Auth::id(),
            'body' => $request->message,
        ]);

        // Sender
        Participant::create([
            'thread_id' => $thread->id,
            'user_id' => Auth::id(),
            'last_read' => new Carbon,
        ]);

        // Recipients
        if ($request->recipients) {
            $thread->addParticipant($request->recipients);
        }

        return redirect()->route('messages');
    }

    /**
     * Adds a new message to a current thread.
     *
     * @param $id
     * @return mixed
     */
    public function update(Request $request, $id)
    {
        $id = Crypt::decrypt($id);
        // Protection contre les failles XSS
        if ( $request->message != null) {
            $request->message = Security::clean($request->message);
        }

        $request->validate([
            'message' => 'required|string|max:255'
        ]);

        try {
            $thread = Thread::findOrFail($id);
        } catch (ModelNotFoundException $e) {
            Session::flash('error_message', 'La discussion n\'existe pas.');

            return redirect()->route('messages');
        }

        $thread->activateAllParticipants();

        // Message
        Message::create([
            'thread_id' => $thread->id,
            'user_id' => Auth::id(),
            'body' => $request->message,
        ]);

        // Recipients
        if ($request->recipients) {
            $thread->addParticipant($request->recipients);
        }

        notify()->info("Message posté");
        return redirect()->route('messages.show', Crypt::encrypt($id));
    }

    public function leave($id, $userId)
    {
        try {
            $thread = Thread::findOrFail($id);
        } catch (ModelNotFoundException $e) {
            Session::flash('error_message', 'La discussion n\'existe pas.');

            return redirect()->route('messages');
        }
        $user = User::findOrFail($userId);
        $thread->removeParticipant($user->id);

        // Message
        Message::create([
            'thread_id' => $thread->id,
            'user_id' => $user->id,
            'body' => $user->pseudo .' a quitté la conversation',
        ]);

        notify()->info("Vous avez quitté la conversation");
        return redirect()->route('messages');
    }

    /**
     * Permet de supprimer la conversation
     * @param [type] $id
     * @return void
     */
    public function delete($id)
    {
        try {
            $thread = Thread::findOrFail($id);
        } catch (ModelNotFoundException $e) {
            Session::flash('error_message', 'La discussion n\'existe pas.');
            return redirect()->route('messages');
        }

        $thread->forceDelete();

        notify()->info("Discussion supprimée");
        return redirect()->back();
    }
}
