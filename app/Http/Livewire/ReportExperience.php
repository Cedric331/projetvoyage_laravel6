<?php

namespace App\Http\Livewire;

use App\Pays;
use App\Report;
use Livewire\Component;
use App\Mail\Signalement;
use App\Experience as Experiences;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Mail;
use GrahamCampbell\Security\Facades\Security;

class ReportExperience extends Component
{

    public $experiences;
    public $reportExperience;
    public $reportMessage;

    /**
     * Récupère les expériences de la page Pays
     *
     * @param Pays $pays
     * @return void
     */
    public function mount($experiences)
    {
        $this->experiences = $experiences;
    }

    /**
     * Reset les propriétés
     *
     * @return void
     */
    private function resetInputFields(){
        $this->reportMessage = '';
        $this->reportExperience = '';
    }

    /**
     * Appel de la fonction pour reset les propriétés
     *
     * @return void
     */
    public function cancel()
    {
        $this->resetInputFields();
    }

    /**
     * Récupère l'expérience visé par le signalement
     *
     * @param [type] $reportExperience
     * @return void
     */
    public function modalExperience($reportExperience)
    {
        $this->reportExperience = Experiences::findOrFail($reportExperience);
    }

    /**
     * Envoi les informations concernant le signalement par mail et le stock dans la DB
     *
     * @return void
     */
    public function submit()
    {
        if (!Auth::check()) {
            abort(403);
        }

             // Protection contre les failles XSS
             if ($this->reportMessage != null) {
                $this->reportMessage = Security::clean($this->reportMessage);
            }
            $this->validate([
                'reportMessage' => 'required|string|max:255',
            ]);

            $report = New Report;
            $report->user_id = Auth::user()->id;
            $report->data = $this->reportExperience;
            $report->message = $this->reportMessage;
            $report->save();

            Mail::to(env('MAIL_USERNAME') )
            ->queue(new Signalement($report));

            $this->resetInputFields();

            session()->flash('message', 'Signalement envoyée');
    }

    /**
     * Envoi les expériences du Pays
     *
     * @return void
     */
    public function render()
    {
        return view('livewire.report-experience', [
            'experiences' => $this->experiences
        ]);
    }
}

