<?php

namespace App\Http\Livewire;

use App\User;
use Livewire\Component;

class SearchUser extends Component
{
    public $search = '';
    public $users = [];

    /**
     * Récupère la recherche de l'utilisateur et effectue une recherche dans les users par Pseudo
     * $users contient ensuite la liste des users suite à la recherche
     * @return void
     */
    public function updatedSearch()
    {
        if(strlen($this->search) > 0) {
            $this->users = User::where('pseudo', 'like', $this->search.'%')
            ->where('email_verified_at', '!=', null)
            ->limit(10)
            ->get();
        }
    }

    public function render()
    {
        return view('livewire.search-user');
    }
}
