<?php

namespace App\Http\Livewire;

use App\Report;
use App\Annonces;
use Livewire\Component;
use App\Mail\Signalement;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Mail;
use GrahamCampbell\Security\Facades\Security;

class ReportAnnonce extends Component
{
    public $annonce;
    public $reportMessage;

    /**
     * Récupère l'annonce au chargement de la page
     *
     * @param [type] $annonce
     * @return void
     */
    public function mount($annonce)
    {
        $this->annonce = $annonce;
    }

    /**
     * Récupère l'annonce ciblé par le signalement
     *
     * @param [type] $id
     * @return void
     */
    public function reportAnnonce($id)
    {
        $this->annonce = Annonces::findOrFail($id);
    }

    /**
     * Enregistre les informations concernant le signalement et l'envoi par mail
     *
     * @return void
     */
    public function submitReport()
    {
        if (!Auth::check()) {
            abort(403);
        }
             // Protection contre les failles XSS
            if ($this->reportMessage != null) {
                $this->reportMessage = Security::clean($this->reportMessage);
            }
            $this->validate([
                'reportMessage' => 'required|string|max:255'
            ]);

            $report = New Report;
            $report->user_id = Auth::user()->id;
            $report->data = $this->annonce;
            $report->message = $this->reportMessage;
            $report->save();

            Mail::to(env('MAIL_USERNAME') )
            ->queue(new Signalement($report));

            $this->resetInputFields();

            session()->flash('success', 'Signalement envoyée');
    }

    /**
     * reset le message concernant le signalement
     *
     * @return void
     */
    private function resetInputFields(){
        $this->reportMessage = '';
    }

    /**
     * Appel de la fonction concernant le signalement
     *
     * @return void
     */
    public function cancel()
    {
        $this->resetInputFields();
    }

    public function render()
    {
        return view('livewire.report-annonce');
    }
}
