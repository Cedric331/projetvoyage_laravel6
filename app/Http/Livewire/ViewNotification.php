<?php

namespace App\Http\Livewire;

use Livewire\Component;
use Illuminate\Support\Facades\Auth;
use Illuminate\Notifications\DatabaseNotification;

class ViewNotification extends Component
{
    public $allNotification;
    public $notifications;
    public $limite = 10;

    /**
     * Récupère les notifications de l'utilisateur
     *
     * @return void
     */
    public function mount()
    {
        $this->allNotification = DatabaseNotification::where('notifiable_id', Auth::user()->id)->get();
    }

    /**
     * Augmente la limite de notification affiché
     *
     * @return void
     */
    public function addLimite()
    {
        $this->limite += 10;
    }

    /**
     * Supprime l'ensemble des notifications
     *
     * @return void
     */
    public function deleteAll()
    {
        foreach ($this->allNotification as $notification) {
            $notification->delete();
        }
    }

    /**
     * Retourne la view avec les notifications par rapport à la limite
     *
     * @return void
     */
    public function render()
    {
        $this->notifications =  $this->allNotification->take($this->limite);
        return view('livewire.view-notification', [
            'notifications' => $this->notifications
            ]);
    }
}
