<?php

namespace App\Http\Livewire;

use Livewire\Component;

class Login extends Component
{
    public $email;
    public $password;

    protected $validationAttributes = [
        'email' => 'adresse email',
        'password' => 'mot de passe'
    ];

    /**
     * Cette fonction permet de valider en temps réel les informations suivant les conditions
     *
     * @param [type] $field contient l'ensemble des données
     * @return void
     */
    public function updated($field)
    {
        $this->validateOnly($field, [
            'email' => ['required', 'email', 'max:100'],
            'password' => ['required', 'min:8'],
        ]);
    }


    public function render()
    {
        return view('livewire.login');
    }
}
