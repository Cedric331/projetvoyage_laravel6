<?php

namespace App\Http\Livewire;

use App\Pays;
use App\User;
use App\Report;
use App\Annonces;
use App\Commentaires;
use Livewire\Component;
use App\LikeCommentaires;
use App\Mail\Signalement;
use App\Notifications\CommentPosted;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Mail;
use App\Notifications\CommentaireLike;
use GrahamCampbell\Security\Facades\Security;
use Illuminate\Notifications\DatabaseNotification;

class Commentaire extends Component
{
    public $page, $type, $nbCommentaire, $editCommentaire, $commentaire, $reportMessage, $message;
    public $limite = 10;

    // $refresh est une méthode de livewire qui permet de rafraîchir
    protected $listeners = [
        'RefreshComments' => '$refresh',
    ];

    /**
     * Récupére les informations de la page
     *
     * @param [type] $page contient l'id de l'élement de la page
     * @param [type] $type contient le type de la page
     * @return void
     */
    public function mount($page, $type)
    {
        $this->page = $page;
        $this->type = $type;
        $this->page();
    }

    /**
     * Récupère la page suivant le type
     * Compte le nombre de commentaire pour la page
     * @return void
     */
    public function page()
    {
        if ($this->type == "App\Pays") {
            $pays = Pays::findOrFail($this->page);
            $this->nbCommentaire = count($pays->commentaires);
        }
        else {
            $annonce = Annonces::findOrFail($this->page);
            $this->nbCommentaire = count($annonce->commentaires);
        }
    }

    /**
     * Lorsque l'utilisateur souhaite charger plus de commentaire
     * cette méthode ajoute +10 à la variable limite pour augmenter le nombre de commentaire visible
     * @return void
     */
    public function plusCommentaire()
    {
        $this->limite += 10;
    }

    /**
     * Reset les propriétés et réactualise la page des commentaires
     *
     * @return void
     */
    private function resetInputFields(){
        $this->editCommentaire = '';
        $this->commentaire = '';
        $this->message = '';

        $this->emit('RefreshComments');
    }

    /**
     * Après vérification, le commentaire est enregistré dans la DB
     * Vérifie s'il s'agit d'une annonce pour envoyer une notification
     * @return void
     */
    public function save()
    {
        if (!Auth::check()) {
            abort(403);
        }

        // Protection contre les failles XSS
        if ($this->message != null) {
            $this->message = Security::clean($this->message);
        }

            $this->validate([
                'message' => 'required|string|max:255',
            ]);

            Commentaires::create([
                'message' => $this->message,
                'user_id' => Auth::user()->id,
                'element_id' => $this->page,
                'type' => $this->type
            ]);

            $this->resetInputFields();

            // Notification pour le créateur de l'annonce si commentaire posté sur une annonce
            if ($this->type == "App\Annonces") {
                $annonce = Annonces::findOrFail($this->page);
                    if (Auth::user() != $annonce->user) {
                        $annonce->user->notify(new CommentPosted($annonce, Auth()->user()));
                    }
            }
    }

    /**
     * Edite le commentaire de l'utilisateur
     *
     * @return void
     */
    public function edit()
    {
        if (Auth::user()->id != $this->commentaire->user_id) {
            abort(403);
        }

        // Protection contre les failles XSS
        if ($this->editCommentaire != null) {
            $this->editCommentaire = Security::clean($this->editCommentaire);
        }

            $this->validate([
                'editCommentaire' => 'required|string|max:255',
            ]);

            $this->commentaire->message = $this->editCommentaire;
            $this->commentaire->save();

            $this->resetInputFields();
    }

    // Appel la fonction pour reset après utilisation du modal d'édition
    public function cancel()
    {
        $this->resetInputFields();
    }

    /**
     * Lors de l'appel du modal, on enregistre les informations du commentaire ciblé
     *
     * @param [type] $id
     * @return void
     */
    public function modal($id)
    {
        if ($id) {
            $this->resetInputFields();
            $this->commentaire = Commentaires::findOrFail($id);
            $this->editCommentaire = $this->commentaire->message;
        }
    }

    /**
     * Supprime le commentaire de l'utilisateur
     *
     * @param [type] $id
     * @return void
     */
    public function delete($id)
    {
        $this->emit('RefreshComments');

        $commentaire = Commentaires::findOrFail($id);

        if (Auth::user()->id == $commentaire->user_id || Auth::user()->isAdmin()) {
            $commentaire->delete();
            return $this->resetInputFields();
        }
        abort(403);
    }

    /**
     * Récupère le commentaire visé par le signalement
     *
     * @param [type] $reportExperience
     * @return void
     */
    public function reportCommentaire($commentaire)
    {
        $this->commentaire = Commentaires::findOrFail($commentaire);
    }

    /**
     * Envoi les informations concernant le signalement par mail et le stock dans la DB
     *
     * @return void
     */
    public function submitReport()
    {
        // Protection contre les failles XSS
        if ($this->reportMessage != null) {
            $this->reportMessage = Security::clean($this->reportMessage);
        }

        $this->validate([
            'reportMessage' => 'required|string|max:255',
        ]);

        $report = new Report;
        $report->data = $this->commentaire;
        $report->message = $this->reportMessage;
        $report->user_id = Auth::user()->id;
        $report->save();

        Mail::to(env('MAIL_USERNAME') )
        ->queue(new Signalement($report));

        session()->flash('success', 'Signalement envoyée');
    }

    /**
     * Permet à l'utilisateur de mettre un like sur un commentaire des annonces
     * $id est l'id du commentaire liké
     * @param [type] $id
     * @return void
     */
    public function like($id)
    {
        if (!Auth::check()) {
            abort(403);
        }
            $commentaire = Commentaires::findOrFail($id);

            $like = new likeCommentaires();
            $like->user_id = Auth::user()->id;
            $like->commentaire_id = $commentaire->id;
            $like->save();
            $this->emit('RefreshComments');

            // Notification
            if (Auth::user()->id != $commentaire->user_id) {
                $commentaire->user->notify(new CommentaireLike($commentaire, $this->page, $this->type, Auth::user()));
            }
    }

    /**
     * Supprime le lke de l'utilisateur sur le commentaire
     * $dislikes contient un tableau de l'ensemble des likes du commentaire
     * @param [type] $dislikes contient l'ensemble des likes du commentaire
     * @return void
     */
    public function dislike($dislikes)
    {
        if (!Auth::check()) {
            abort(403);
        }
        foreach ($dislikes as $dislike){
            // Récupère le like appartenant à l'utilisateur auth et le supprime
            if($dislike['user_id'] == Auth::user()->id)
            {
                $like = likeCommentaires::findOrFail($dislike['id']);

                // Récupère la notification en lien avec le like
                $notification = DatabaseNotification::where('type', 'App\Notifications\CommentaireLike')
                ->where('data', '{"element":'.$this->page.',"type":"App\\\Pays","CommentId":'.$dislike['commentaire_id'].',"userName":"'.Auth::user()->pseudo.'"}')
                ->orWhere('data', '{"element":'.$this->page.',"type":"App\\\Annonces","CommentId":'.$dislike['commentaire_id'].',"userName":"'.Auth::user()->pseudo.'"}')
                ->where('read_at', null)->first();

                // Vérifie si la notification existe et n'est pas null
                // Puis la supprime si true
                if ($notification){
                $notification->delete();
                }

                // Supprime le like
                $like->delete();
            }
        }
            $this->emit('RefreshComments');
    }

    /**
     * Vérification si l'utilisateur à déja mis un like sur le commentaire
     * $like contient un tableau de l'ensemble des likes du commentaire
     * @param [type] $like
     * @return void
     */
    public function verificationLike($likes)
    {
        if (Auth::check() && $likes) {
            foreach ($likes as $like) {
                if($like->user_id == Auth::user()->id)
                {
                    return true;
                }
            }
            return false;
        }
    }

    /**
     * Méthode qui retourne le component commentaire avec en paramètre les commentaires de la page sélectionnée
     *
     * @return void
     */
    public function render()
    {
        if ($this->type == "App\Pays") {
            $pays = Pays::findOrFail($this->page);
            $commentaires = $pays->commentaires->take($this->limite);
        }
        else {
            $annonce = Annonces::findOrFail($this->page);
            $commentaires = $annonce->commentaires->take($this->limite);
        }

        return view('livewire.commentaire', [
            'commentaires' => $commentaires,
        ]);
    }

}
