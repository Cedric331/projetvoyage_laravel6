<?php

namespace App\Http\Livewire;

use App\Report;
use Livewire\Component;
use App\Mail\Signalement;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Mail;
use GrahamCampbell\Security\Facades\Security;

class ReportConversation extends Component
{

    public $thread;
    public $reportThread;

    public function mount($thread)
    {
        $this->thread = $thread;
    }

    /**
     * Envoi les informations concernant le signalement par mail et le stock dans la DB
     *
     * @return void
     */
    public function submitReport()
    {

        if (!Auth::check()) {
            abort(403);
        }
             // Protection contre les failles XSS
             if ($this->reportThread != null) {
                $this->reportThread = Security::clean($this->reportThread);
            }

            $this->validate([
                'reportThread' => 'required|string|max:255',
            ]);

            $report = New Report;
            $report->user_id = Auth::user()->id;
            $report->data = $this->thread;
            $report->message = $this->reportThread;
            $report->save();

            Mail::to(env('MAIL_USERNAME') )
            ->queue(new Signalement($report));

            session()->flash('success', 'Signalement envoyée');
    }

    public function render()
    {
        return view('livewire.report-conversation');
    }
}
