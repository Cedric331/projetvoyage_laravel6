<?php

namespace App\Http\Livewire;

use App\Mois;
use App\Pays;
use App\Saisons;
use App\Periodes;
use App\Continents;
use Livewire\Component;
use GrahamCampbell\Security\Facades\Security;

class PaysSearch extends Component
{

    public $listes, $budget, $continent, $saison, $mois, $countPays;
    public $limite = 6;

    /**
     * Récupère les paramètres de l'utilisateur
     *
     * @param [type] $budget
     * @param [type] $continent
     * @param [type] $saison
     * @param [type] $mois
     * @return void
     */
    public function mount($budget, $continent, $saison, $mois)
    {
        if ($budget != null) {
            $this->budget = Security::clean($budget);
        }

        if ($continent != null) {
            $this->continent = Security::clean($continent);
        }

        if ($saison != null) {
            $this->saison = Security::clean($saison);
        }

        if ($mois != null) {
            $this->mois = Security::clean($mois);
        }

        $this->search();
    }

    /**
     * Ajoute +3 à la propriété limite
     * Permet d'afficher plus de résultat sur la page
     *
     * @return void
     */
    public function count()
    {
        $this->limite += 3;
    }

    /**
     * Fonction de recherche suivant les critères sélectionnés
     *
     * @return void
     */
    public function search()
    {
        $continent = Continents::where('nom_continent', $this->continent)->first();

        if ($continent != null) {
            $this->listes = Pays::where('prix','<', $this->budget)
                        ->where('pays.continent_id', $continent->id)
                        ->get();
        }
        else
        {
            $this->listes = Pays::where('prix','<', $this->budget)->get();
        }

        if ($this->saison != null && $this->mois != null) {

            $mois = Mois::where('mois', $this->mois)->first();

            $saison = Saisons::where('saison', $this->saison)->first();

            $periodes = Periodes::where('saison_id', $saison->id)
                                ->where('mois_id', $mois->id)
                                ->get()->all();

                 if (count($periodes) != 0)
                 {
                     // Stock les ids des pays qui correspondent à la période sélectionnée
                    $array = collect([]);
                    foreach ($periodes as $periode)
                    {
                        $pays = $periode->pays;
                        $array->push($pays->id);
                    }

                    $this->pays = Pays::find($array->all());

                        if ($continent != null) {
                            $this->pays = $this->pays->where('continent_id', '=', $continent->id);
                        }

                    $this->listes = $this->pays->where('prix','<', $this->budget);
                 }
                 else
                 {
                    $this->listes = [];
                 }
        }

        if (count($this->listes) != 0) {
            $this->countPays = $this->listes->count();
        }
    }

    /**
     * Si la propriété public de continent est modifié, on rappel la fonction search
     *
     * @return void
     */
    public function updatedContinent()
    {
        // Protection contre les failles XSS
        if ($this->continent != null) {
            $this->continent = Security::clean($this->continent);
        }

        $this->search();
    }

    /**
     * Si la propriété public de mois est modifié, on rappel la fonction search
     * Informe l'utilisateur que la propriété saison doit également être sélectionné
     * @return void
     */
    public function updatedMois()
    {
        // Protection contre les failles XSS
        if ($this->mois != null) {
            $this->mois = Security::clean($this->mois);
        }

        if (empty($this->saison) && !empty($this->mois)) {

             session()->flash('mois', 'Vous devez également sélectionner une saison');
        }
        else if (empty($this->mois) && !empty($this->saison))
        {
            session()->flash('mois', 'Vous devez sélectionner un mois avec la saison');
        }
            $this->search();

    }

    /**
     * Si la propriété public de saison est modifié, on rappel la fonction search
     * Informe l'utilisateur que la propriété mois doit également être sélectionné
     * @return void
     */
    public function updatedSaison()
    {
        // Protection contre les failles XSS
        if ($this->saison != null) {
            $this->saison = Security::clean($this->saison);
        }

        if (empty($this->mois) && !empty($this->saison)) {

             session()->flash('saison', 'Vous devez également sélectionner un mois');
        }
        else if (empty($this->saison) && !empty($this->mois))
        {
            session()->flash('saison', 'Vous devez sélectionner une saison avec le mois');
        }
            $this->search();
    }

    /**
     * Si la propriété public de budget est modifié, on rappel la fonction search
     *
     * @return void
     */
    public function updatedBudget()
    {
        // Protection contre les failles XSS
         if ($this->budget != null) {
            $this->budget = Security::clean($this->budget);
        }

        $this->search();
    }

    /**
     * Permet de retourner la page avec le résultat de la recherche
     * Récupère dans la liste un nombre limité de pays
     * @return void
     */
    public function render()
    {
        if (count($this->listes) != 0) {
            $this->pays = $this->listes->take($this->limite)->sortBy('nom_pays');
        }
        else {
            $this->pays = [];
        }

        return view('livewire.pays-search',[
            'liste' => $this->pays
        ]);
    }
}
