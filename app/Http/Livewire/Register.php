<?php

namespace App\Http\Livewire;

use Livewire\Component;

class Register extends Component
{
    public $pseudo;
    public $email;
    public $password;

    /**
     * Cette fonction permet de valider en temps réel les informations suivant les conditions
     *
     * @param [type] $field contient l'ensemble des données
     * @return void
     */
    public function updated($field)
    {
        $this->validateOnly($field, [
            'pseudo' => ['required', 'alpha_dash', 'string','min:4', 'max:15','unique:users,pseudo'],
            'email' => ['required', 'string', 'email', 'max:100', 'unique:users,email'],
            'password' => ['required', 'string', 'min:8'],
            'password_confirmation' => ['required', 'string', 'min:8'],
        ]);
    }

    public function render()
    {
        return view('livewire.register');
    }
}
