<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Commentaires extends Model
{
    protected $fillable = [
        'message','user_id','element_id', 'type',
    ];

    /**
     * Retourne les likes du commentaire
     *
     * @return void
     */
    public function like()
    {
        return $this->hasMany(LikeCommentaires::class, 'commentaire_id');
    }

    /**
     * Récupère le Pays du commentaire
     *
     * @return void
     */
    public function pays()
    {
        return $this->belongsTo(Pays::class);
    }

    /**
     * Récupère l'annonce du commentaire
     *
     * @return void
     */
    public function annonce()
    {
        return $this->belongsTo(Annonces::class);
    }

    /**
     * Récupère l'utilisateur qui a écrit le commentaire
     *
     * @return void
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
