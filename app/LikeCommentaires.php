<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LikeCommentaires extends Model
{
            /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'commentaire_id',
    ];

    /**
     * Retourne l'utilisateur qui a liké
     *
     * @return void
     */
    public function user()
    {
        return $this->belongsTo(Users::class);
    }
}
