@extends('layouts/app')
@section('fond', 'accueil')

@section('content')

<!-- Formulaire -->
<section class="container cadre">
   <h1>Votre prochaine destination de vacance</h1>
<p>Remplissez le questionnaire pour trouver les pays qui vous correspondent</p>
   <form method="POST" action="{{ route('search.pays') }}">
    @csrf
      <div class="form-group">
         <label for="continent">Choix du continent</label>
         <select class="form-control" id="continent" name="continent">
            <option value="" selected>-- Aucune préférence --</option>
            <option value="Europe">Europe</option>
            <option value="Afrique">Afrique</option>
            <option value="Asie">Asie</option>
            <option value="Amérique du Nord">Amérique du Nord</option>
            <option value="Amérique du Sud">Amérique du Sud</option>
            <option value="Oceanie">Océanie</option>
         </select>
      </div>
      <div class="form-group">
        <label for="mois">Mois : </label>
        <select class="form-control @error('mois') is-invalid @enderror" id="mois" name="mois">
           <option value="" selected>-- Aucune préférence --</option>
           <option value="Janvier">Janvier</option>
           <option value="Février">Février</option>
           <option value="Mars">Mars</option>
           <option value="Avril">Avril</option>
           <option value="Mai">Mai</option>
           <option value="Juin">Juin</option>
           <option value="Juillet">Juillet</option>
           <option value="Aout">Août</option>
           <option value="Septembre">Septembre</option>
           <option value="Octobre">Octobre</option>
           <option value="Novembre">Novembre</option>
           <option value="Decembre">Décembre</option>
        </select>
        @error('mois')
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
        @enderror
     </div>
     <div class="form-group">
        <label for="saison">Saison : </label>
        <select class="form-control @error('saison') is-invalid @enderror" id="saison" name="saison">
           <option value="" selected>-- Aucune préférence --</option>
           <option value="ete">Été - Saison des pluies</option>
           <option value="hiver">Hiver - Saison sèche</option>
           <option value="automne">Automne</option>
           <option value="printemps">Printemps</option>
        </select>
        @error('saison')
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
        @enderror
     </div>
      <div class="form-group">
         <label for="budget">Budget moyen/personne: <span id="AffichePrix">600</span>€</label>
         <input type="range" min="600" max="3000" value="600" class="form-control-range progress-bar bg-success" id="budget" name="budget" oninput="document.getElementById('AffichePrix').textContent=value">
      </div>
      <button type="submit" class="btn btn-primary">Recherche</button>
   </form>

</section>

@endsection
