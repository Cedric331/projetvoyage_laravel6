@extends('layouts/app')
@section('fond', 'annonce')

@section('content')

    @guest

    <div class="jumbotron container mt-5">
        <p class="lead">Si vous souhaitez trouver des gens qui partagent la passion des voyages pour discuter et peut-être trouver des personnes pour vous accompagner, vous êtes au bon endroit.
        </p>
        <hr class="my-4">
        <p>Pour accéder au contenu, vous devez être <a class="text-primary" href="{{ route('login') }}">connecté</a>.</p>
      </div>

    @else
    <div class="container my-5 py-5 z-depth-1 bg-white">
        <section class="px-md-5 mx-md-5 text-center text-lg-left text-dark">
          <div class="row">
            <div class="col-md-6 mb-4 mb-md-0">
              <p>Si vous souhaitez trouver des gens qui partagent la passion des voyages pour discuter et peut-être trouver des personnes pour vous accompagnez, vous êtes au bon endroit.</p>
              <p><strong>Comment cela fonctionne?</strong><br>C'est très simple, vous pouvez interagir avec les annonces via les commentaires ou discuter directement avec la personne à l'origine de l'annonce. Bien sûr si vous avez envie vous pouvez poster votre propre annonce.</p>
              <div class="card bg-white" style="border: 0px;">
                  <blockquote class="blockquote mb-0">
                    <p class="text-dark">​"L'impulsion du voyage est l'un des plus encourageants symptômes de la vie"</p>
                    <footer class="blockquote-footer bg-white text-dark">Citation d'Agnès Repplier, <cite title="Source Title">essayiste américaine</cite></footer>
                  </blockquote>
              </div>
            </div>
            <div class="col-md-6 mb-4 mb-md-0">
              <div class="view overlay z-depth-1-half">
                <img src="https://mdbootstrap.com/img/Photos/Others/img%20(28).jpg" class="img-fluid"
                  alt="">
              </div>
            </div>
          </div>
        </section>
      </div>

      <div class="jumbotron bg-white container-fluid" style="margin: 100px 0px;">
        <h1 class="centre">
            Liste des annonces
        </h1>
        <div class="centre">
            <a class="btn btn-success" href="{{ route('annonceFormulaire') }}">Créer une annonce</a>
        </div>
      </div>

@foreach ( $annonces as $annonce )
      <div class="container mt-5 bg-white col-8">
        <section class="dark-grey-text">
          <div class="row align-items-center">
            <div class="col-lg-5 col-xl-4 p-0">
              <div class="view overlay rounded z-depth-1-half mb-lg-0">
                <img class="img-fluid ml-0" src="{{ $annonce->image_principal }}" alt="Image du pays {{ $annonce->nom_pays }}">
              </div>
            </div>
            <div class="col-lg-5 col-xl-6 m-auto p-2">
              <h4 class="font-weight-bold mb-3"><strong>{{ $annonce->titre }}</strong></h4>
              <p class="dark-grey-text">@if ( $annonce->nom_pays ) Pays: {{ $annonce->nom_pays }}@endif</p>
              <p class="dark-grey-text">@if ( $annonce->duree ) Durée du séjour: {{ $annonce->duree }}@endif</p>
              <p class="dark-grey-text">@if ( $annonce->date ) Date de départ: {{ date('d-m-Y', strtotime($annonce->date)) }}@endif</p>
              <p>par {!! lienCompte($annonce, 'text-capitalize font-weight-bold') !!}{{ $annonce->pseudo }}</a>, {{ $annonce->created_at->diffForHumans() }}</p>
              <a href="{{ route('annonce.view', [Crypt::encrypt($annonce->id), $annonce->nom_pays] ) }}" class="btn btn-success btn-md mx-0 btn-rounded mt-2">En savoir plus</a>
              @if (Auth::user() == $annonce->user)
                  <a href="{{ route('annonce.update', Crypt::encrypt($annonce->id) ) }}" class="btn btn-primary mt-2">Modifier mon annonce</a>
              @endif
            </div>
            <div class="mr-1 ml-2 col-lg-1 col-xl-1 float-right">
                @livewire('report-annonce', ['annonce' => $annonce])
            </div>
          </div>
        </section>
      </div>
@endforeach


<div class="ml-5 mb-5">
    {{ $annonces->links() }}
</div>
    @endguest

@endsection
