@extends('layouts/app')
@section('fond', 'annonce')

@section('content')

<!-- Modal confirmation supprimer compte -->
<div class="modal fade" id="deleteAnnonce" tabindex="-1" role="dialog" aria-labelledby="deleteAnnonce" aria-hidden="true">
    <div class="modal-dialog modal-notify" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <p class="heading lead">Supprimer votre annonce ?</p>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true" class="white-text">&times;</span>
          </button>
        </div>
          <div class="modal-footer justify-content-center">
            <form action="{{ route('annonce.delete', $annonce->id) }}" method="POST">
                @csrf
                @method('DELETE')
                <button type="submit" class="btn btn-danger ml-3">Supprimer mon annonce</button>
            </form>
              <a type="button" class="btn btn-success waves-effect" data-dismiss="modal">Non, merci</a>
          </div>
      </div>
    </div>
  </div>
  <!-- Fin Modal confirmation supprimer compte -->

<section class="form-gradient container mb-5">
    <div class="card bg-white">
        <div class="header pt-3 peach-gradient">
            <div class="row d-flex justify-content-center">
                <h3 class="white-text mb-3 pt-3 font-weight-bold">Modfification d'une Annonce</h3>
            </div>
        </div>
        <hr>
        @if(session()->has('error'))
        <div class="alert alert-danger container w-50">
            {{ session('error') }}
        </div>
        @endif
        <div class="card-body mx-4 mt-4">
            <form action="{{ route('annonce.update.patch', Crypt::encrypt($annonce->id)) }}" method="POST">
                @method('PATCH')
                @csrf
                <div class="md-form">
                    <label for="titre" class="text-dark @error('titre') is-invalid @enderror">Titre du Voyage:</label>
                    <input type="text" id="titre" name="titre" value="{{ $annonce->titre }}" class="form-control">
                    @error('titre')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>

                <div class="md-form pb-1 pb-md-3">
                    <label for="pays" class="text-dark">*Pays pour votre séjour:</label>
                    <select name="pays" id="pays" class="form-control @error('pays') is-invalid @enderror">
                        <option value="{{ $pays->id }}">{{ $pays->nom_pays }}</option>
                        @foreach ( $listes as $pays)
                        <option value="{{ $pays->id }}">{{ $pays->nom_pays }}</option>
                        @endforeach
                    </select>
                    @error('pays')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>

                <div class="md-form pb-1 pb-md-3">
                    <label for="date" class="text-dark">*Date de départ:</label>
                    <input type="date" name="date" id="date" value="{{ $annonce->date }}" class="form-control @error('date') is-invalid @enderror">
                    @error('date')
                    <span class="invalid-feedback" role="alert">
                        <strong>La date doit être une date postérieure à aujourd'hui</strong>
                    </span>
                    @enderror
                </div>

                <div class="md-form pb-1 pb-md-3">
                    <label for="duree" class="text-dark">*Durée du séjour:</label>
                    <input type="text" name="duree" id="duree"
                        value="{{ $annonce->duree }}" class="form-control @error('duree') is-invalid @enderror">
                        @error('duree')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                </div>

                <div class="form-group shadow-textarea">
                    <label for="description" class="text-dark">Description de votre séjour:</label>
                    <textarea class="form-control z-depth-1 @error('description') is-invalid @enderror" name="description" id="description" rows="3">{{ $annonce->description }}</textarea>
                    @error('description')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>

                <div class="row d-flex align-items-center mb-4">
                    <div class="col-md-1 col-md-5 d-flex align-items-start">
                        <div>
                            <button type="submit" class="btn btn-outline-success btn-rounded waves-effect">Valider</button>
                            <a class="btn btn-outline-primary btn-rounded waves-effect ml-3" href="{{ route('annonce') }}">Annuler</a>
                        </div>
                    </div>
                </div>
                <div class="align-items-end">
                    <button type="button" class="btn btn-outline-danger btn-rounded waves-effect" value="Supprimer" data-toggle="modal" data-target="#deleteAnnonce">Supprimer mon annonce</button>
                </div>
            </form>
        </div>
        <em class="text-danger mb-2 ml-2">*Facultatif</em>
    </div>
</section>

@endsection
