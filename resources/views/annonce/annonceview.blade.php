@extends('layouts/app')
@section('fond', 'annonce')

@section('content')

<div class="container my-5">
    <section class="dark-grey-text bg-white p-2">
      <div class="row align-items-center">
        <div class="col-lg-5 col-xl-4">
          <div class="view overlay rounded z-depth-1-half mb-lg-0 mb-4">
            <img class="img-fluid m-auto" src="{{ $pays->image_principal }}" alt="Image de {{ $pays->nom_pays }}">
            <a>
              <div class="mask rgba-white-slight"></div>
            </a>
          </div>
        </div>
        <div class="col-lg-7 col-xl-8">
          <h4 class="font-weight-bold mb-3"><strong>{{$annonce->titre }}</strong></h4>
          <p class="dark-grey-text" style="word-break:break-word">{{ $annonce->description }}</p>
          <p class="dark-grey-text">Destination: <a href="{{ route('pays.pays', $pays->nom_pays) }}" style="color:rgb(42, 130, 156);" target="_blank">{{ $pays->nom_pays }}</a></p>
          @if ($annonce->date)
          <p class="dark-grey-text"> Date de départ : {{ date('d-m-Y', strtotime($annonce->date)) }}</p>
          @endif
          @if ($annonce->duree)
          <p class="dark-grey-text">Durée du séjour : {{ $annonce->duree }}</p>
          @endif
          <p>par {!! lienCompte($user, 'font-size:18px; color:blue;') !!}{{ $user->pseudo }}</a>, {{ $annonce->created_at->diffForHumans() }}</p>
              @if (Auth::user()->pseudo == $user->pseudo)
                  <a href="{{ route('annonce.update', Crypt::encrypt($annonce->id) ) }}" class="btn btn-primary my-2">Modifier mon annonce</a>
              @endif
        </div>
      </div>
    </section>
  </div>

  @livewire('commentaire',['page' => $annonce->id, 'type' => get_class($annonce)])
@endsection
