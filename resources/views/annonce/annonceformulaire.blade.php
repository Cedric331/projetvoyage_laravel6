@extends('layouts/app')
@section('fond', 'annonce')

@section('content')

<section class="form-gradient container mb-5">
    <div class="card bg-white">
        <div class="header pt-3 peach-gradient">
            <div class="row d-flex justify-content-center">
                <h3 class="white-text mb-3 pt-3 font-weight-bold">Création d'une Annonce de Voyage</h3>
            </div>
        </div>
        <hr>
        @if(session()->has('error'))
        <div class="alert alert-danger container w-50">
            {{ session('error') }}
        </div>
        @endif
        <div class="card-body mx-4 mt-4">
            <form action="{{ route('annonceFormulaire.post') }}" method="POST">
                @csrf
                <div class="md-form">
                    <label for="titre" class="text-dark @error('titre') is-invalid @enderror">Titre du Voyage:</label>
                    <input type="text" id="titre" name="titre" value="{{ old('titre') }}" class="form-control">
                    @error('titre')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>

                <div class="md-form pb-1 pb-md-3">
                    <label for="pays" class="text-dark">Pays pour votre séjour:</label>
                    <select name="pays" id="pays" class="form-control @error('pays') is-invalid @enderror">
                        <option value="">- Veuillez choisir -</option>
                        @foreach ( $listes as $pays)
                        <option value="{{ $pays->id }}">{{ $pays->nom_pays }}</option>
                        @endforeach
                    </select>
                    @error('pays')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>

                <div class="md-form pb-1 pb-md-3">
                    <label for="date" class="text-dark">*Date de départ:</label>
                    <input type="date" name="date" id="date" value="{{ old('date') }}" class="form-control @error('date') is-invalid @enderror">
                    @error('date')
                    <span class="invalid-feedback" role="alert">
                        <strong>La date doit être une date postérieure à aujourd'hui</strong>
                    </span>
                    @enderror
                </div>

                <div class="md-form pb-1 pb-md-3">
                    <label for="duree" class="text-dark">*Durée du séjour:</label>
                    <input type="text" name="duree" placeholder="2 jours, 1 semaine, 2 mois..." id="duree"
                        value="{{ old('duree') }}" class="form-control @error('duree') is-invalid @enderror">
                        @error('duree')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                </div>

                <div class="form-group shadow-textarea">
                    <label for="description" class="text-dark">Description de votre séjour:</label>
                    <textarea class="form-control z-depth-1 @error('description') is-invalid @enderror" name="description" id="description" rows="3"
                        placeholder="Décrivez votre séjour, le nombre, prix estimé, les activités, donner un maximum de détails...">{{ old('description') }}</textarea>
                    @error('description')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>

                <div class="row d-flex align-items-center mb-4">
                    <div class="col-md-1 col-md-5 d-flex align-items-start">
                        <div class="text-center">
                            <button type="submit" class="btn btn-success btn-rounded z-depth-1a">Créer</button>
                            <a class="btn btn-danger ml-3" href="{{ route('annonce') }}">Annuler</a>
                        </div>
                    </div>
                </div>
            </form>
        </div>
        <em class="text-danger mb-2 ml-2">*Facultatif</em>
    </div>
</section>

@endsection
