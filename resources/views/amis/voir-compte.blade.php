@extends('layouts.app')
@section('fond', 'compte')
@section('content')


<div class="container-fluid" style="max-width: 800px;">
    <div class="card-deck">
        <div class="card mb-5 p-3 centre cadre">

            {{-- Si l'utilisateur est amis avec l'utilisateur authentifié --}}
            @if($amis)
            <h1>Compte de {{ $user->pseudo }}</h1>
            <div class="row">
                <div class="col-md-12">
                    <div class="centre mb-4">
                        {!! avatarUser($user) !!}
                    </div>
                </div>
            </div>
            <h6 class="font-weight-normal text-uppercase font-small grey-text mb-4">Compte de {{ $user->pseudo }}</h6>
            <p class="grey-text mx-auto mb-2">Inscrit {{$user->created_at->diffForHumans()}}</p>
            <a class="btn btn-primary mb-4" href="/messages/create">Envoyer un message</a>

            {{-- Pays Favoris  --}}
            <div class="container pt-5 my-5 z-depth-1 cadre">
                <section class="p-md-3 mx-md-5 text-lg-left">
                    <h2 class="text-center font-weight-bold mb-4 pb-1">Pays favoris de {{ $user->pseudo }}</h2>
                    <hr class="my-4">
                    <div class="row">
                        @if(count($favoris) != 0)
                        @foreach ($favoris as $pays)
                        <div class="col-lg-4 col-sm-6 my-5">
                            <div class="row d-flex align-items-center">
                                <div class="col-5 avatar w-100 white d-flex justify-content-center align-items-center">
                                    <img src="{{ $pays->image_principal }}"
                                        class="img-fluid rounded-circle z-depth-1" />
                                </div>
                                <div class="col-7">
                                    <h6 class="font-weight-bold pt-2"></h6>
                                    <h4 class="text-muted">
                                        <a class="text-dark" href="{{ route('pays.pays',  $pays->nom_pays) }}">
                                            {{ $pays->nom_pays }}
                                        </a>
                                    </h4>
                                </div>
                            </div>
                        </div>
                        @endforeach
                        @else
                        <p class="m-auto font-weight-bold mb-4 pb-1">Aucun favoris</p>
                        @endif
                    </div>
                </section>
            </div>

            {{-- Expérience  --}}
            <div class="container my-5">
                <section class="dark-grey-text mb-5">
                    <h2 class="text-center font-weight-bold mb-4 pb-1">Expérience de {{ $user->pseudo }}</h2>
                    <hr class="my-4">
                    @if(count($favoris) != 0)
                    @foreach ($experiences as $experience)
                    <div class="media avis mt-2 p-4 cadre">
                        {!! verificationAvatar($user) !!}
                        <div class="media-body">
                            <h5 class="font-weight-bold">{{ $experience->pseudo }}</h5>
                            <h3><a class="text-dark"
                                    href="{{ route('pays.pays', $experience->nom_pays) }}">{{ $experience->nom_pays }}</a>
                            </h3>
                            <div class="card-data">
                                <ul class="list-unstyled mb-1">
                                    <li class="font-small"><strong>{{ $experience->titre_experience }}</strong></li>
                                    <li>
                                        @for ($i = 0; $i<$experience->note_experience; $i++)
                                            <strong class="note">☆</strong>
                                            @endfor
                                    </li>
                                </ul>
                            </div>
                            <p class="article">{{ $experience->description_experience }}</p>
                        </div>
                    </div>
                    @endforeach
                    @else
                    <p class="m-auto font-weight-bold mb-4 pb-1 text-center">Aucune expérience partagée</p>
                    @endif
                </section>
            </div>
            {{-- Supprime un ami  --}}
            <form action="{{ route('amis-supprimer', $user->pseudo ) }}" method="POST">
                @method('DELETE')
                @csrf
                <input type="submit" class="btn btn-danger" value="Supprimer">
            </form>
            @else
            {{-- Vérifie si $demandeEnCours est true ou false suivant si une demande d'amis est déjà en cours --}}
            @if ($demandeEnCours)
            <div class="container mt-5">
                <section class="text-center dark-grey-text p-5">
                    <div class="row">
                        <div class="col-md-12 mb-4">
                            <div class="centre mb-4">
                                {!! avatarUser($user) !!}
                            </div>
                        </div>
                    </div>
                    <h6 class="font-weight-normal text-uppercase font-small grey-text mb-4">Compte de
                        {{ $user->pseudo }}</h6>
                    <p class="grey-text mx-auto mb-5">Inscrit {{$user->created_at->diffForHumans()}}</p>
                    <hr class="w-header my-4">
                    <p>Demande d'amis en cours</p>
                    {{-- Si l'utilisateur authentifié n'est pas à l'origine de la demande d'amis  --}}
                    @if (count(Auth::user()->verificationDemandeAmis($user->id)) != 0)
                        <form
                            action="{{ route('amis.refuser', Auth::user()->verificationDemandeAmis($user->id)->first()->id ) }}"
                            method="POST">
                            @method('DELETE')
                            @csrf
                            <input type="submit" class="btn btn-danger" value="Refuser la demande d'amis">
                        </form>
                    @else
                    <form action="{{ route('delete.demande', $user->id) }}" method="POST">
                        @csrf
                        @method('DELETE')
                        <input type="submit" class="btn btn-danger" value="Annuler la demande d'amis">
                    </form>
                    @endif
                </section>
            </div>
            @else

            @if ($user->id == Auth::user()->id)
            <h1>Compte de {{ $user->pseudo }}</h1>
            <a href="{{ route('compte',  $user->id) }}" class="btn btn-primary">Voir mon compte</a>
            @else
            <div class="container mt-5">
                <section class="text-center dark-grey-text p-5">
                    <div class="row">
                        <div class="col-md-12 mb-4">
                            <div class="centre mb-4">
                                {!! avatarUser($user) !!}
                            </div>
                        </div>
                    </div>
                    <h6 class="font-weight-normal text-uppercase font-small grey-text mb-4">Compte de
                        {{ $user->pseudo }}</h6>
                    <p class="grey-text mx-auto mb-5">Inscrit {{$user->created_at->diffForHumans()}}</p>
                    <hr class="w-header my-4">
                    <form action="{{ route('ajouter.amis',  $user->id) }}" method="POST">
                        @csrf
                        <input type="submit" class="btn btn-primary" value="Ajouter en amis">
                    </form>
                </section>
            </div>
            @endif
            @endif
            @endif
        </div>
    </div>
</div>

@endsection
