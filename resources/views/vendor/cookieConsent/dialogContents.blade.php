<div class="js-cookie-consent cookie-consent">

    <div class="card">
        <div class="card-body">
            <span class="cookie-consent__message">
                {!! trans('Ce site utilise des cookies pour améliorer votre expérience. Pour en savoir plus veuillez lire notre <a href="CGU#cookie" style="color: rgb(95, 147, 206);">politique de confidentialité</a>') !!}
            </span>

            <button class="js-cookie-consent-agree cookie-consent__agree btn btn-primary">
                {{ trans('D\'accord') }}
            </button>
        </div>
      </div>


</div>
