<div>
    <div class="md-form mt-3">
        <label for="pseudo" class="col-form-label text-md-right">{{ __('Pseudo : ') }}</label>
        <input id="pseudo" type="text" wire:model.lazy="pseudo" class="form-control @error('pseudo') is-invalid @enderror" name="pseudo" value="{{ old('pseudo') }}" autocomplete="pseudo" autofocus>
        @error('pseudo')
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
        @enderror
      </div>

      <div class="md-form mt-3">
        <label for="email" class="col-form-label text-md-right">{{ __('Adresse email : ') }}</label>
        <input id="email" type="email" wire:model.lazy="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" autocomplete="email">
        @error('email')
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
        @enderror
      </div>

      <div class="md-form mt-3">
        <label for="password" class=" col-form-label text-md-right">{{ __('Mot de passe : ') }}</label>
        <input id="password" type="password" name="password" wire:model.lazy="password" class="form-control @error('password') is-invalid @enderror">
        @error('password')
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
        @enderror
      </div>

      <div class="md-form mt-3">
        <label for="password_confirmation" class=" col-form-label text-md-right">{{ __('Confirmer votre mot de passe :') }}</label>
        <input id="password_confirmation" type="password" class="form-control" name="password_confirmation" autocomplete="new-password">
        @error('password_confirmation')
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
        @enderror
    </div>

</div>
