<div wire:ignore.self class="modal fade" id="edit" tabindex="-1" role="dialog"
    aria-labelledby="Editer un commentaire" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header text-center">
            <h2>Éditer un commentaire</h2>
                <button type="button" class="close" wire:click.prevent="cancel()" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true close-btn">×</span>
                </button>
            </div>
            <div class="modal-body mx-3">
                    <label for="form8">Commentaire</label>
                    <textarea id="form8" wire:model.lazy="editCommentaire" class="md-textarea form-control" rows="4"></textarea>
            </div>
            <div class="modal-footer d-flex justify-content-center">
                <button type="button" class="btn btn-secondary" data-dismiss="modal" wire:click.prevent="cancel()">Annuler</button>
                <button type="button" class="btn btn-primary" data-dismiss="modal" wire:click.prevent="edit()">Modifier</button>
            </div>
        </div>
    </div>
</div>
