<div class="container mt-5">
    <section class=" text-dark mb-2">
        <div class="container m-auto row my-3">
        @foreach ($experiences as $experience)
        <div class="media avis cadre container mt-4 col-12 col-md-5 col-xl-3" style="max-width: 500px; word-break:break-word">
            {!! verificationAvatar($experience->user)!!}
            <div class="media-body">

                    @if($experience->user != Auth::user() && Auth::check())
                    <a id="navbarDropdown" class="float-right text-dark" href="#" role="button" data-toggle="dropdown"
                        aria-haspopup="true" aria-expanded="false" v-pre>
                        <i class="fas fa-angle-down"></i>
                    </a>

                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                        <button wire:click="modalExperience({{ $experience->id }})"
                            class="dropdown-item my-auto text-center" data-toggle="modal" data-target="#report">
                            Signaler
                        </button>
                    </div>
                    @endif

                <h5 class="font-weight-bold text-dark">{!! lienCompte($experience->user)
                    !!}{{ $experience->user->pseudo }}</a></h5>
                <div class="card-data">
                    <ul class="list-unstyled mb-1">
                        <li class="font-small">
                            <strong>{{ $experience->titre_experience }}</strong>
                        </li>
                        <li>
                            @for ( $i = 0; $i < $experience->note_experience; $i++)
                                <strong class="note">☆</strong>
                                @endfor
                        </li>
                    </ul>
                </div>
                <p class="article">{{ $experience->description_experience }}</p>
                @if (Auth::check() && Auth::user()->id == $experience->user_id)
                <a href="{{ route('partage-updateExp',  Crypt::encrypt($experience->id)) }}" class="btn btn-primary m-auto">Modifier mon
                    expérience</a>
                @endif
            </div>
        </div>
        @endforeach
    </div>
    </section>

    {{-- Modal --}}
    <div>
        <div wire:ignore.self class="modal fade" id="report" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
            aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header text-center">
                        <h4 class="modal-title w-100 font-weight-bold">Signaler</h4>
                        <button type="button" class="close" wire:click.prevent="cancel()" data-dismiss="modal"
                            aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>

                    <div class="alert alert-info text-center" wire:loading wire:target="submit">
                        <div class="spinner-border text-primary" role="status">
                            <span class="sr-only">Chargement...</span>
                        </div>
                    </div>
                    @if (session()->has('message'))
                    <div class="alert alert-success text-center">
                        {{ session('message') }}
                    </div>
                    @endif

                    <div class="modal-body mx-3">
                        <div class="md-form">
                            <i class="fas fa-pencil prefix grey-text"></i>
                            <label data-error="wrong" data-success="right" for="form8">Raison:</label>
                            <select wire:model="reportMessage" class="browser-default custom-select">
                                <option value="">Merci de choisir la raison</option>
                                <option value="Contenu indésirable">Contenu indésirable</option>
                                <option value="Discrous haineux">Discrous haineux</option>
                                <option value="Harcèlement">Harcèlement</option>
                                <option value="Autre">Autre</option>
                            </select>
                            @if ($errors->has('reportMessage'))
                            <span class="help-block" style="color:rgb(201, 45, 45);">
                                <strong>{{ 'Veuillez sélectionner la raison' }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>
                    <div class="modal-footer d-flex justify-content-center">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal"
                            wire:click.prevent="cancel()">Annuler</button>
                        <button type="button" wire:click.prevent="submit" class="btn btn-primary">Envoyer<i
                                class="fas fa-paper-plane-o ml-1"></i></button>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
