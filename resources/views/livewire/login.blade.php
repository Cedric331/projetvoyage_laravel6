<div class="container cadre" style="max-width: 25%">

    <h2 class="centre">Connexion</h2>
    <hr>
    <form method="POST" action="{{ route('login') }}">
        @csrf

        <div class="form-group row p-2">
            <label for="email" class="col-form-label">{{ __('Adresse email : ') }}</label>
            <input id="email" type="email" wire:model.lazy="email"
                class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}"
                autocomplete="email">
            @error('email')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
            @enderror
        </div>

        <div class="form-group row p-2">
            <label for="password" class=" col-form-label">{{ __('Mot de passe : ') }}</label>
            <input id="password" type="password" name="password" wire:model.lazy="password"
                class="form-control  @error('password') is-invalid @enderror">
            @error('password')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
            @enderror
        </div>

        <div class="form-group row">
            <div class="col-md-6">
                <div class="form-check">
                    <label class="form-check-label" for="remember">
                        <input class="form-check-input" type="checkbox" name="remember" id="remember"
                            {{ old('remember') ? 'checked' : '' }}>
                        {{ "Se souvenir" }}
                    </label>
                </div>
            </div>
        </div>


        <div class="form-group row mt-4 mb-0 centre">
            <div class="mt-2 centre col-md-12">
                <button type="submit" id="bouton" class="btn btn-outline-primary">
                    {{ __('Connexion') }}
                </button>
            </div>
            <div class="mt-2 col-12 centre">
                <a class="btn btn-link text-primary" href="{{ route('password.request') }}">
                    {{ __('Mot de passe oublié ?') }}
                </a>
                <a class="btn btn-link text-primary" href="{{ route('register') }}">
                    {{ __('S\'inscrire ?') }}
                </a>
            </div>
        </div>
    </form>


</div>
