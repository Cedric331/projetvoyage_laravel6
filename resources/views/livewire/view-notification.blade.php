<div>
    <div class="cardExperience ombre container my-5" style="max-width:400px;">
        <h1 class="centre">Mes notifications</h1>
            <hr>
                @if ($allNotification->count() == 0)
                    <div>
                        <strong class="centre p-2">Aucune notification</strong>
                    </div>
                @else
                    @foreach ($notifications as $notification)
                    <div class="m-auto p-2">
                        @if ($notification->type == "App\Notifications\CommentPosted")
                            <a href="{{ route('annonce.view.notification', [$notification->data['annonceId'], $notification->id]) }}">
                                {{ $notification->data['userName']}} à posté un commentaire sur votre annonce

                                <strong>{{ $notification->data['annonceTitle'] }}</strong>

                                <p class="ml-2" style="font-size:13px; color:grey;">
                                    {{ $notification->created_at->diffForHumans() }}
                                </p>
                            </a>
                            <hr>
                        @elseif($notification->type == "App\Notifications\CommentaireLike" && $notification->data['type'] == "App\Annonces")
                            <a href="{{ route('commentaireAnnonce.notification', [$notification->data['element'], $notification->id]) }}">
                                {{ $notification->data['userName']}} aime votre commentaire
                                <p class="ml-2" style="font-size:13px; color:grey;">
                                    {{ $notification->created_at->diffForHumans() }}
                                </p>
                            </a>
                            <hr>
                        @elseif($notification->type == "App\Notifications\CommentaireLike" && $notification->data['type'] == "App\Pays")
                            <a href="{{ route('commentairePays.notification', [$notification->data['element'], $notification->id]) }}">
                                {{ $notification->data['userName']}} aime votre commentaire
                                <p class="ml-2" style="font-size:13px; color:grey;">
                                    {{ $notification->created_at->diffForHumans() }}
                                </p>
                            </a>
                            <hr>
                        @else
                            <a href="{{ route('compte.amis.notification', $notification->id) }}">
                                {{ $notification->data['userName']}} vous a envoyé(e) une demande d'amis
                                <p class="ml-2" style="font-size:13px; color:grey;">
                                    {{ $notification->created_at->diffForHumans() }}
                                </p>
                            </a>
                            <hr>
                        @endif
                    </div>
                    @endforeach

                    <div class="d-flex justify-content-between">

                        @if ($limite < $allNotification->count())
                            <button class="btn btn-success my-2" wire:click="addLimite">En voir plus</button>
                        @endif

                            <button class="btn btn-danger my-2" wire:click="deleteAll">Tout supprimer</button>
                    </div>
                @endif
            </div>
</div>
