
<div class="dropdown" id="searchUser">
    <input type="search" wire:model="search"  class="form-control" placeholder="Rechercher un utilisateur" style="border-top:none;">
    @if(strlen($search) > 0)
        @if (count($users) > 0)
            @foreach ($users as $user)
            {!! lienCompte($user) !!}
                <li class="list-group-item p-1 bg-dark text-white text-center listeUser" class="dropdown-menu m-0 p-0" style="border:none;">
                    {{ $user->pseudo }}
                </li>
            </a>
            @endforeach
        @else
        <li class="list-group-item p-1 bg-dark text-center listeUser" style="border:none;">
            <span class="dropdown-header text-white">Aucun résultat</span>
        </li>
        @endif
    @endif
</div>




