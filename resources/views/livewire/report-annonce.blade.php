<div>
    @if($annonce->user != Auth::user() && Auth::check())
    <a id="navbarDropdown" class="float-right text-dark" href="#" role="button" data-toggle="dropdown"
        aria-haspopup="true" aria-expanded="false" v-pre>
        <i class="fas fa-angle-down"></i>
    </a>

    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
        <button type="button" wire:click="reportAnnonce({{ $annonce->id }})"
            class="dropdown-item my-auto text-center" data-toggle="modal" data-target="#report">
            Signaler
        </button>
    </div>
    @endif

    {{-- Modal --}}
<div>
    <div wire:ignore.self class="modal fade" id="report" tabindex="-1" role="dialog"
        aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header text-center">
                    <h4 class="modal-title w-100 font-weight-bold">Signaler</h4>
                    <button type="button" class="close" wire:click.prevent="cancel()" data-dismiss="modal"
                        aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div class="alert alert-info text-center" wire:loading wire:target="submitReport">
                    <div class="spinner-border text-primary" role="status">
                        <span class="sr-only">Chargement...</span>
                    </div>
                </div>
                @if (session()->has('success'))
                <div class="alert alert-success text-center">
                    {{ session('success') }}
                </div>
                @endif

                <div class="modal-body mx-3">
                    <div class="md-form">
                        <i class="fas fa-pencil prefix grey-text"></i>
                        <label data-error="wrong" data-success="right" for="form8">Raison:</label>
                        <select wire:model="reportMessage" class="browser-default custom-select">
                            <option value="">Merci de choisir la raison</option>
                            <option value="Contenu indésirable">Contenu indésirable</option>
                            <option value="Discrous haineux">Discrous haineux</option>
                            <option value="Harcèlement">Harcèlement</option>
                            <option value="Autre">Autre</option>
                        </select>
                        @if ($errors->has('reportMessage'))
                        <span class="help-block" style="color:rgb(201, 45, 45);">
                            <strong>{{ 'Veuillez sélectionner la raison' }}</strong>
                        </span>
                        @endif
                    </div>
                </div>
                <div class="modal-footer d-flex justify-content-center">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal"
                        wire:click.prevent="cancel()">Annuler</button>
                    <button type="button" wire:click.prevent="submitReport" class="btn btn-primary">Envoyer<i
                            class="fas fa-paper-plane-o ml-1"></i></button>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
