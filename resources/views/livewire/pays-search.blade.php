<div>

    <nav class="navbar navbar-light bg-light mb-5">
        <div class="row col-12">
            <div class="col-6 col-lg-3 my-auto">
                <label>Continent :</label>
                <select class="form-control" wire:model="continent">
                    <option value="">Aucune préférence</option>
                    <option value="Europe">Europe</option>
                    <option value="Afrique">Afrique</option>
                    <option value="Asie">Asie</option>
                    <option value="Amérique du Nord">Amérique du Nord</option>
                    <option value="Amérique du Sud">Amérique du Sud</option>
                    <option value="Oceanie">Océanie</option>
                 </select>
            </div>
            <div class="col-6 col-lg-3 my-auto">
                <label>Mois :</label>
                <select class="form-control" wire:model="mois">
                    <option value="">Aucune préférence</option>
                    <option value="Janvier">Janvier</option>
                    <option value="Février">Février</option>
                    <option value="Mars">Mars</option>
                    <option value="Avril">Avril</option>
                    <option value="Mai">Mai</option>
                    <option value="Juin">Juin</option>
                    <option value="Juillet">Juillet</option>
                    <option value="Aout">Août</option>
                    <option value="Septembre">Septembre</option>
                    <option value="Octobre">Octobre</option>
                    <option value="Novembre">Novembre</option>
                    <option value="Decembre">Décembre</option>
                 </select>
                 <div class="mt-2">
                    @if (session()->has('mois'))
                        <div class="alert alert-info">
                            {{ session('mois') }}
                        </div>
                    @endif
                </div>
            </div>
            <div class="col-6 col-lg-3 my-auto">
                <label>Saison :</label>
                <select class="form-control" wire:model="saison">
                    <option value="" selected>Aucune préférence</option>
                    <option value="ete">Été - Saison des pluies</option>
                    <option value="hiver">Hiver - Saison sèche</option>
                    <option value="automne">Automne</option>
                    <option value="printemps">Printemps</option>
                 </select>
                 <div class="mt-2">
                    @if (session()->has('saison'))
                        <div class="alert alert-info">
                            {{ session('saison') }}
                        </div>
                    @endif
                </div>
            </div>
            <div class="form-group col-6 col-lg-3 my-auto">
                <label for="budget">Budget moyen/personne: <span id="AffichePrix">{{ $budget }}</span>€</label>
                <input type="range" min="600" max="3000" class="form-control-range" wire:model="budget">
            </div>
          </div>
      </nav>


    @if (empty($liste))
    <div class="container centre m-auto">
        <div class="card row align-self-center mt-5">
            <div class="card-body">
                <h1>Aucun resultat pour cette recherche</h1>
            </div>
        </div>
    </div>
    @else
    <div class="container centre m-auto">
        <div class="row row-cols-1 row-cols-sm-2 row-cols-md-3">
            @foreach ($liste as $pays)
            <div class="col liste mb-4">
                <div class="card cardExperience ombre">

                    <div class="view overlay">
                        <a href="{{ route('pays.pays', $pays->nom_pays) }}"><img class="card-img-top"
                                src="{{ $pays->image_principal }}" alt="{{ $pays->nom_pays }}"></a>
                    </div>

                    <div class="card-body">
                        <h2 class="card-title centre" style="color:black;">{{ $pays->nom_pays }}</h2>
                        <hr>
                        <a href="{{ route('pays.pays', $pays->nom_pays) }}" class="centre btn btn-info">Voir ce Pays</a>
                    </div>

                </div>
            </div>
            @endforeach
        </div>
    </div>
    @if ($countPays > $limite)
    <div class="container m-auto centre">
        <button class="btn btn-primary my-3 centrer" wire:click="count">Afficher plus de résultat</button>
    </div>
    @endif
@endif
</div>
