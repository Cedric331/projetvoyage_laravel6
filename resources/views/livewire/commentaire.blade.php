<div>
    @foreach($commentaires as $commentaire)
    <ul class="list-unstyled cadre text-break" style="max-width: 500px;">
        <li class="media">
            {!! verificationAvatar($commentaire)!!}

            <div class="media-body" style="word-break:break-word">
                {{-- Signaler un commentaire --}}
                @auth
                <a id="navbarDropdown" class="float-right text-dark" href="#" role="button" data-toggle="dropdown"
                    aria-haspopup="true" aria-expanded="false" v-pre>
                    <i class="fas fa-angle-down"></i>
                </a>

                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">

                    @if($commentaire->user != Auth::user())
                    <button type="button" wire:click="reportCommentaire({{ $commentaire->id }})"
                        class="dropdown-item my-auto text-center" data-toggle="modal" data-target="#reportComment">
                        Signaler
                    </button>
                    @endif

                    @if($commentaire->user == Auth::user())
                    <button class="dropdown-item my-auto text-center btn btn-default btn-rounded mb-4"
                        wire:click="modal({{ $commentaire->id }})" data-toggle="modal"
                        data-target="#edit">Editer</button>
                    @endif
                    @if($commentaire->user == Auth::user() || Auth::user()->isAdmin())
                    <button class="dropdown-item my-auto text-center"
                        wire:click="delete({{ $commentaire->id }})">Supprimer</button>
                    @endif

                </div>
                @endauth

                <h5 class="mb-1 font-weight-bold text-dark">
                    {!! lienCompte($commentaire) !!}{{ $commentaire->pseudo }}</a></h5>
                <p style="text-align: start;">{{ $commentaire->message }}</p>

                <hr class="mb-0">

                <em style="font-size:13px; color:grey;">{{ $commentaire->created_at->diffForHumans() }}</em>
                @if ($commentaire->created_at != $commentaire->updated_at)
                <em class="float-right" style="font-size:13px; color:grey;">modifié
                    {{ $commentaire->updated_at->diffForHumans() }}</em>
                @endif
                <br>
                {{-- Verification si l'utilisateur à déjà mis un like sur le commentaire --}}
                @auth
                @if( $this->verificationLike($commentaire->like))
                <em class="my-0" style="font-size:13px; color:grey;">
                    <button class="btn btn-outline-info btn-sm" style="border:none;"
                        wire:click="dislike({{ $commentaire->like }})">
                        Je n'aime plus
                        @if (count($commentaire->like) != 0)
                            ({{ count($commentaire->like) }})
                        @endif
                    </button>
                </em>
                @else
                <em class="my-0" style="font-size:13px; color:grey;">
                    <button class="btn btn-outline-info btn-sm" style="border:none;"
                        wire:click="like({{ $commentaire->id }})">
                        J'aime
                        @if (count($commentaire->like) != 0)
                        ({{ count($commentaire->like) }})
                        @endif
                    </button>
                </em>
                @endif
                @endauth
            </div>
        </li>
    </ul>
    @endforeach

    @if($limite < $nbCommentaire)
        <button class="btn btn-primary centre m-auto" wire:click="plusCommentaire">Voir plus de commentaire</button>
    @endif



        <div class="container m-auto" style="max-width: 500px;">
            @if(session()->has('message'))
            <div class="alert alert-success">
                {{ session('message') }}
            </div>
            @endif

            @if(session()->has('error'))
            <div class="alert alert-danger">
                {{ session('error') }}
            </div>
            @endif

            @if(Auth::check() && Auth::user()->isVerified())
            <form wire:submit.prevent="save" class="mt-3">
                @csrf
                <div class="form-group shadow-textarea">
                    <textarea class="form-control z-depth-1 @error('message') is-invalid @enderror" wire:model="message"
                        id="message" rows="5" placeholder="Votre commentaire ici..."></textarea>
                    @error('message')
                    <span class="invalid-feedback ml-5" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
                <div class="modal-footer justify-content-center">
                    <input type="submit" class="btn btn-info" value="Poster votre commentaire">
                </div>
            </form>
            @elseif(Auth::check() && !Auth::user()->isVerified())
            <div class="cadre d-flex justify-content-center">
                <p class="block">Pour poster des commentaires, vous devez valider l'email de vérification</p>
            </div>
            @else
            <div class="cadre d-flex justify-content-center">
                <p class="block">Vous devez être <a class="text-primary"
                        href="{{ route('login') }}">{{ __('connecté') }}</a> pour poster un commentaire</p>
            </div>
            @endif
        </div>
        @include('livewire.editCommentaire')

        {{-- Modal --}}
        <div>
            <div wire:ignore.self class="modal fade" id="reportComment" tabindex="-1" role="dialog"
                aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header text-center">
                            <h4 class="modal-title w-100 font-weight-bold">Signaler</h4>
                            <button type="button" class="close" wire:click.prevent="cancel()" data-dismiss="modal"
                                aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>

                        <div class="alert alert-info text-center" wire:loading wire:target="submitReport">
                            <div class="spinner-border text-primary" role="status">
                                <span class="sr-only">Chargement...</span>
                            </div>
                        </div>
                        @if (session()->has('success'))
                        <div class="alert alert-success text-center">
                            {{ session('success') }}
                        </div>
                        @endif
                        <div class="modal-body mx-3">
                            <div class="md-form">
                                <i class="fas fa-pencil prefix grey-text"></i>
                                <label data-error="wrong" data-success="right" for="form8">Raison:</label>
                                <select wire:model="reportMessage" class="browser-default custom-select">
                                    <option value="">Merci de choisir la raison</option>
                                    <option value="Contenu indésirable">Contenu indésirable</option>
                                    <option value="Discrous haineux">Discrous haineux</option>
                                    <option value="Harcèlement">Harcèlement</option>
                                    <option value="Autre">Autre</option>
                                </select>
                                @if ($errors->has('reportMessage'))
                                <span class="help-block" style="color:rgb(201, 45, 45);">
                                    <strong>{{ 'Veuillez sélectionner la raison' }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                        <div class="modal-footer d-flex justify-content-center">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal"
                                wire:click.prevent="cancel()">Annuler</button>
                            <button type="button" wire:click.prevent="submitReport" class="btn btn-primary">Envoyer<i
                                    class="fas fa-paper-plane-o ml-1"></i></button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
</div>
