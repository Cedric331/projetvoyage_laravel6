@extends('layouts/app')
@section('fond', 'accueil')

@section('content')

<div class="container my-5">

    <style>
        .border-top {
            border: 5px solid #33b5e5 !important;
            border-top-left-radius: .25rem !important;
            border-top-right-radius: .25rem !important;
        }

    </style>

    <!--Section: Content-->
    <section class="text-center dark-grey-text mb-5">

        <div class="card">
            <div class="card-body rounded-top border-top p-5">

                <!-- Section heading -->
                <h3 class="font-weight-bold my-4">Formulaire de contact</h3>
                <!-- Section description -->
                <form class="mb-4 mx-md-5" action="{{ route('contact.post') }}" method="POST">
                    @csrf
                    <div class="row">
                        <div class="col-md-6 mb-4">
                            <input type="text" id="name" name="name" value="{{ old('name') }}" class="form-control @error('name') is-invalid @enderror" placeholder="Votre nom">
                            @error('name')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                        <div class="col-md-6 mb-4">
                            <input type="email" id="email" name="email" value="{{ old('email') }}" class="form-control @error('email') is-invalid @enderror" placeholder="Votre email">
                            @error('email')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12 mb-4">
                            <input type="text" id="subject" name="sujet" value="{{ old('sujet') }}" class="form-control @error('sujet') is-invalid @enderror" placeholder="Sujet">
                            @error('sujet')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group mb-4">
                                <textarea class="form-control rounded @error('message') is-invalid @enderror" name="message" value="{{ old('message') }}" id="message" rows="3"
                                    placeholder="Votre message"></textarea>
                                @error('message')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                            <div class="text-center">
                                <button type="submit" class="btn btn-info btn-md">Envoyer</button>
                            </div>
                        </div>
                    </div>

                </form>

            </div>
        </div>
    </section>
</div>

@endsection
