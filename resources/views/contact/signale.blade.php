<!DOCTYPE html>
<html lang="fr">
  <head>
    <meta charset="utf-8">
  </head>
  <body>
    <h2>Réception d'une prise de contact avec les éléments suivants :</h2>
    <ul>
      <li><strong>Sujet:</strong> : Signalement</li>
      <li><strong>Information:</strong> : {{ $contact['data'] }}</li>
      <li><strong>Message:</strong> : {{ $contact['message'] }}</li>
    </ul>
  </body>
</html>
