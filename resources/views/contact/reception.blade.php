<!DOCTYPE html>
<html lang="fr">
  <head>
    <meta charset="utf-8">
  </head>
  <body>
    <h2>Réception d'une prise de contact avec les éléments suivants :</h2>
    <ul>
      <li><strong>Nom</strong> : {{ $contact['name'] }}</li>
      <li><strong>Email</strong> : {{ $contact['email'] }}</li>
      <li><strong>Sujet</strong> : {{ $contact['sujet'] }}</li>
      <li><strong>Message</strong> : {{ $contact['message'] }}</li>
    </ul>
  </body>
</html>
