@extends('layouts/app')
@section('fond', 'erreur')
@section('content')

<div class="container my-5 py-5 z-depth-1">

    <section class="px-md-5 mx-md-5 text-center dark-grey-text bg-dark">
        <h1 class="font-weight-bold text-white">Erreur 403</h1>
        <h3 class="font-weight-bold text-white">Action non autorisé.</h3>
    </section>

  </div>
@endsection
