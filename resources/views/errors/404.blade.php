@extends('layouts/app')
@section('fond', 'erreurs')
@section('content')

<div class="container my-5 py-5 z-depth-1">

    <section class="px-md-5 mx-md-5 text-center dark-grey-text">
        <img src="https://mdbootstrap.com/img/Others/404_mdb.png" alt="Error 404" class="img-fluid mb-4" >
        <h3 class="font-weight-bold text-white">Oops! Désolé cette page n'existe pas.</h3>
    </section>

  </div>
@endsection
