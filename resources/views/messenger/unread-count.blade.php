<?php $count = Auth::user()->newThreadsCount(); ?>
@if($count > 0)
    <span class="label label-info"> {{ $count }}</span>
@endif
