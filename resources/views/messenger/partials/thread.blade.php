<?php $class = $thread->isUnread(Auth::id()) ? 'alert-info' : ''; ?>

<div class="media alert {{ $class }}">
    <a href="{{ route('messages.show', Crypt::encrypt($thread->id)) }}" class="mr-2 text-dark">Titre: {{ $thread->subject }}
        <h4 class="media-heading">
            ({{ $thread->userUnreadMessagesCount(Auth::id()) }} non lus )
        </h4>
    </a>

    <div class="ml-2">
        <p>Créateur: {{ $thread->creator()->pseudo }}</p>
        <p>Participants: {{ $thread->participantsString() }}</p>
    </div>
    @if($thread->creator()->id == Auth::user()->id )
        <form action="{{ route('messages.delete', $thread->id) }}" method="POST">
            @csrf
            @method('DELETE')
            <button class="ml-5 btn btn-danger">Supprimer la conversation</button>
        </form>
    @else
        <form action="{{ route('messages.leave', [$thread->id, Auth::user()->id]) }}" method="POST">
        @csrf
        @method('PATCH')
            <button class="ml-5 btn btn-danger">Quitter la conversation</button>
        </form>
    @endif
</div>
