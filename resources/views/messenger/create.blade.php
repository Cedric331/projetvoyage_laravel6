@extends('layouts.master')

@section('messagerie')

<a class="btn btn-primary" href="/messages">Revenir à la boîte de reception</a>
<form action="{{ route('messages.store') }}" method="post">
    @csrf
    <div class="form-row">
        <div class="form-group col-md-7">
            <label class="control-label">Titre</label>
            <input type="text" class="form-control @error('subject') is-invalid @enderror" name="subject"
                placeholder="Sujet" value="{{ old('subject') }}">
            @error('subject')
            <div class="alert alert-danger">{{ $message }}</div>
            @enderror
            <label class="control-label">Message</label>
            <textarea name="message" rows="8"
                class="form-control @error('message') is-invalid @enderror">{{ old('message') }}</textarea>
            @error('message')
            <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>

        <div class="form-group col-md-4 ml-2">
            <h1 class="mb-2 text-white text-center">Liste d'amis</h1>
            @if($users->count() > 0)

            @foreach($users as $user)
            <div class="row ml-5 mt-1">
                <div class="d-flex mr-1 avatar">
                     {!! verificationAvatar($user) !!}
                </div>
                <label title="{{ $user->pseudo }}">
                    <input type="checkbox" name="recipients[]" value="{{ $user->id }}">
                    {{ $user->pseudo }}
                </label>
            </div>
            @endforeach

            @else
                <h5 class="text-center mt-5">-- Liste d'amis vide --</h5>
            @endif

        </div>


        <div class="form-group col-md-7 col-12">
            <button type="submit" class="btn btn-primary form-control">Envoyer</button>
        </div>
    </div>
</form>
@endsection
