@extends('layouts.master')

@section('messagerie')
    @if (Session::has('error_message'))
    <div class="alert alert-danger" role="alert">
        {{ Session::get('error_message') }}
    </div>
    @endif
    <a class="btn btn-primary my-3" href="/messages/create">Créer un nouveau message</a>
    @each('messenger.partials.thread', $threads, 'thread', 'messenger.partials.no-threads')
@endsection
