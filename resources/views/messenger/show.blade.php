@extends('layouts.master')

@section('messagerie')

@livewire('report-conversation', ['thread' => $thread])

<a class="btn btn-primary" href="/messages">Revenir à la boîte de reception</a>

<hr>
<div class="col-md-6 col-12 text-center m-auto my-custom-scrollbar" id="scroll">
    <h1 class="text-white">{{ $thread->subject }}</h1>

    <div class="col-md-12 text-white">
        <h2>Participant(s): {{$participants}}</h2>
    </div>
    <hr>
    <div class="container my-5">

        @foreach ($thread->messages as $message)
        <section class="my-2">
            <div class="card p-2 cadre" style="margin:20px auto;">
                <div class="card-body">
                    <div class="media mt-4">
                        {!! verificationAvatar($message->user) !!}

                        <div class="media-body">
                            <h6 class="font-weight-bold text-left">{!! lienCompte($message->user)
                                !!}{{ $message->user->pseudo }}</a><span
                                    class="small text-muted float-right pr-2">{{ $message->created_at->diffForHumans() }}</span>
                            </h6>
                            <p class="mb-1 font-weight-normal text-dark text-break text-justify">{{ $message->body }}
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        @endforeach
    </div>
    <h2 class="text-white">Poster un nouveau message</h2>
    <form action="{{ route('messages.update', Crypt::encrypt($thread->id)) }}" method="POST">
        @method('PUT')
        @csrf

        <!-- Message Form Input -->
        <div class="form-group">
            <textarea name="message" rows="5"
                class="form-control @error('message') is-invalid @enderror">{{ old('message') }}</textarea>
            @error('message')
            <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>
        @if($users->count() > 0)
        <div class="checkbox">
            @foreach($users as $user)
            <input type="checkbox" name="recipients[]" value="{{ $user->id }}">
            <label title="pseudo">{{ $user->pseudo }}</label>
            @endforeach
        </div>
        @endif

        <!-- Submit Form Input -->
        <div class="form-group">
            <button type="submit" class="btn btn-primary form-control">Envoyer</button>
        </div>
    </form>
</div>

<script>
    element = document.querySelector('#scroll');
    element.scrollTop = element.scrollHeight;

</script>
@endsection
