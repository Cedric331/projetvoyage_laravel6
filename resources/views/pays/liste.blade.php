@extends('layouts/app')
@section('fond', 'accueil')
@section('content')


    @if ($listes->count() == 0)
        <div class="container centre m-auto">
            <div class="card row align-self-center mt-5">
                <div class="card-body">
                    <h1>Aucun resultat pour cette recherche</h1>
                </div>
            </div>
        </div>

    @else

        <div class="container centre m-auto">
            <div class="row row-cols-1 row-cols-sm-2 row-cols-md-3 mt-5 mb-5">
                @foreach ($listes as $liste)
                    <div class="col liste mb-4">
                        <div class="card cardExperience ombre">

                            <div class="view overlay">
                                <a href="{{ route('pays.pays', $liste->nom_pays) }}"><img class="card-img-top" src="{{ $liste->image_principal }}" alt="{{ $liste->nom_pays }}"></a>
                            </div>

                            <div class="card-body">
                                <h2 class="card-title centre" style="color:black;">{{ $liste->nom_pays }}</h2>
                                <hr>
                                <a href="{{ route('pays.pays', $liste->nom_pays) }}" class="centre btn btn-info">Voir ce Pays</a>
                            </div>

                        </div>
                    </div>
                @endforeach
                {{ $listes->links() }}
            </div>
        </div>

    @endif

@endsection
