@extends('layouts/app')

@section('fond', 'accueil')

@section('content')

@livewire('pays-search', [
          'budget' => $budget,
          'continent' => $continent,
          'saison' => $saison,
          'mois' => $mois])

@endsection
