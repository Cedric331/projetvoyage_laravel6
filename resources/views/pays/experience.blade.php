@extends('layouts/app')
@section('fond', 'accueil')
@section('content')


<!-- Modal -->
<div class="modal fade" id="confirmDeleteExperience" tabindex="-1" role="dialog" aria-labelledby="deleteExperience" aria-hidden="true">
    <div class="modal-dialog modal-notify" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <p class="heading lead">Supprimer votre expérience</p>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true" class="white-text">&times;</span>
          </button>
        </div>
          <div class="modal-footer justify-content-center">
              <form action="{{ route('partage-delete', $experience->id) }}" method="POST" class="w-50 centre">
                  @csrf
                  @method('DELETE')
                  <button type="submit" class=" btn btn-danger">Supprimer mon expérience</button>
                </form>
              <a type="button" class="btn btn-success waves-effect" data-dismiss="modal">Non, merci</a>
          </div>
      </div>
    </div>
  </div>
  <!-- Fin Modal -->

<div class="card container my-5 cadre">

    <h1 class="card-header text-center py-4">
      Partage d'expérience
    </h1>

    <div class="card-body m-auto col-md-8 px-lg-5 pt-0">
        <form action="{{ route('partage-update', $experience->id) }}" method="POST">
            @csrf
            @method('PUT')
            <label for="input">Titre: </label>
            <input type="text" id="input" name="titre" class="form-control" value="{{ $experience->titre_experience }}">

            <label for="description">Message: </label>
            <textarea type="text" name="description" id="description" class="form-control md-textarea" rows="3">{{ $experience->description_experience }}</textarea>

{{-- {{ dd($experience->note_experience) }} --}}
            <div class="rating m-auto">
                <input name="note" id="e5" value="5" type="radio" @if($experience->note_experience == 5) checked @endif>
                <label for="e5">☆</label>
                <input name="note" id="e4" value="4" type="radio" @if($experience->note_experience == 4) checked @endif>
                <label for="e4">☆</label>
                <input name="note" id="e3" value="3" type="radio" @if($experience->note_experience == 3) checked @endif>
                <label for="e3">☆</label>
                <input name="note" id="e2" value="2" type="radio" @if($experience->note_experience == 2) checked @endif>
                <label for="e2">☆</label>
                <input name="note" id="e1" value="1" type="radio" @if($experience->note_experience == 1) checked @endif>
                <label for="e1">☆</label>
            </div>

            @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif

            <button class="btn btn-outline-success btn-rounded btn-block z-depth-0 mb-4 waves-effect" type="submit">Modifier mon expérience</button>
        </form>
            <button type="button" class=" btn btn-danger" value="Supprimer" data-toggle="modal" data-target="#confirmDeleteExperience">Supprimer mon expérience</button>
    </div>
</div>
@endsection
