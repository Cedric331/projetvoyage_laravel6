@extends('layouts/app')

@section('fond', strtolower($pays->nom_pays))

@section('content')

<div class="container cadre">
    <div class="row flex wrap">
        <div class="col-lg-6 centre">
            <img class="imagePays" src="{{ $pays->image_principal }}" alt="image {{ $pays->nom_pays }}">
        </div>
        <div class="col-lg-6 col-md-12">
            <h1 class="centre">{{ $pays->nom_pays }}</h1>
            <hr>
            <p>
                {!! $pays->description_pays  !!}
            </p>
        </div>
    </div>


    <div class="mt-3 accordion" id="accordionExample">
        <div class="card blanc">
            <div class="card-header" id="headingOne">
                <h2 class="mb-0">
                    <button class="btn btn-link btn-block collapsed text-dark" type="button" data-toggle="collapse"
                        data-target="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
                        Le climat ?
                    </button>
                </h2>
            </div>
            <div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#accordionExample">
                <div class="card-body text-dark">
                    {!! $pays->description_climat !!}
                </div>
            </div>
        </div>

        <div class="card blanc">
            <div class="card-header" id="headingTwo">
                <h2 class="mb-0">
                    <button class="btn btn-link btn-block collapsed text-dark" type="button" data-toggle="collapse"
                        data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                        Meilleure période pour partir ?
                    </button>
                </h2>
            </div>
            <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
                <div class="card-body text-dark">
                    {!! $pays->description_partir !!}
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row">

            <div class="container z-depth-1">
                <section class="text-center dark-grey-text">
                  <div class="row text-center d-flex justify-content-center mt-5">
                    <div class="col-lg-2 col-md-6 mb-4">
                        <h5 class="font-weight-normal mb-3">Drapeau</h5>
                        <img class="img-thumbnail p-1" src="{{ restCountry($pays->iso)->flag }}" alt="Drapeaux du pays">
                      </div>
                    <div class="col-lg-2 col-md-6 mb-4">
                      <h5 class="font-weight-normal mb-3">Capitale</h5>
                      <p class="mb-0 text-uppercase font-weight-bold">{{ restCountry($pays->iso)->capital }}</p>
                    </div>
                    <div class="col-lg-2 col-md-6 mb-4">
                      <h5 class="font-weight-normal mb-3">Continent</h5>
                      <p class="mb-0 font-weight-bold">{{ $pays->continent->nom_continent }}</p>
                    </div>
                    <div class="col-lg-2 col-md-6 mb-4">
                      <h5 class="font-weight-normal mb-3">Poulation</h5>
                      <p class="mb-0 font-weight-bold">{{ restCountryPop($pays->iso) }}</p>
                    </div>
                    <div class="col-lg-2 col-md-6 mb-4">
                      <h5 class="font-weight-normal mb-3">Budget moyen</h5>
                      <p class="mb-0 font-weight-bold">{{ $pays->prix }} €/personne pour une semaine</p>
                    </div>
                  </div>
                </section>
              </div>

        @auth
            <section class="m-auto">
                <div class="row text-center d-flex justify-content-center mt-5">
                    <div class="col-xs-6 mr-5 mt-1">
                        <a href="{{ route('partage', $pays->nom_pays) }}" class="btn btn-outline-primary">Partager mon expérience de Voyage</a>
                    </div>

                    @if (Auth::user()->isFavoris($pays->id))
                    <div class="col-xs-6 mr-5 mt-1">
                        <form method="POST" action="{{ route('delete.favoris', $pays->id) }}">
                            @method('DELETE')
                            @csrf
                            <button type="submit" class="btn btn-outline-primary">Supprimer des favoris</button>
                        </form>
                    </div>

                    @else

                    <div class="col-xs-6 mr-5 mt-1">
                        <form method="POST" action="{{ route('pays.favoris', $pays->nom_pays) }}">
                            @method('POST')
                            @csrf
                            <button type="submit" class="btn btn-outline-primary">Ajouter ce pays dans les favoris</button>
                        </form>
                    </div>
                    @endif

                </div>
            </section>
        @endauth
        </div>
    </div>
</div>


<div class="container m-auto row my-3">
    @foreach ( $tourismes as $tourisme )
    <div class="container mt-4 col-12 col-md-6 col-xl-4">
        <div class="card blanc">
            <div class="card-body p-0">
                <img src="{{ $tourisme->image_tourisme }}" class="card-img-top"
                    alt="Image {{ $tourisme->titre_tourisme }}">
                <h5 class="card-title m-2 text-dark">{{ $tourisme->titre_tourisme }}</h5>
                <hr>
                <p class="card-text m-2 text-dark">{{ $tourisme->description_tourisme }}</p>
                <a href="{{ $tourisme->lien_tourisme }}" class="btn btn-outline-success m-2" target="blank">En savoir plus</a>
            </div>
        </div>
    </div>
    @endforeach
</div>

@livewire('report-experience', ['experiences' => $experiences])

@livewire('commentaire',['page' => $pays->id, 'type' => get_class($pays)])


@endsection
