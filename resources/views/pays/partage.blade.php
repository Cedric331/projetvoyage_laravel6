@extends('layouts/app')
@section('fond', 'accueil')
@section('content')

<div class="card container cadre my-5">

    <h1 class="bg-light card-header text-center py-4">
        Partage d'expérience
    </h1>

    <div class="card-body col-md-8 m-auto px-lg-5 pt-0">

        <form class="md-form" method="POST" action="{{ route('partage-store', $pays->nom_pays) }}">
            @csrf
            <label for="titre">Titre : </label>
            <input type="text" id="titre" name="titre" value="{{ old('titre') }}" class="form-control" placeholder="Titre">

            <label class="mt-5" for="description">Description de votre séjour: </label>
            <textarea type="text" name="description" id="description" placeholder="Description de votre expérience" class="form-control md-textarea" rows="3">{{ old('description') }}</textarea>

            <div class="rating m-auto form-group" value="4">
                <input name="note" id="e5" value="5" type="radio">
                <label for="e5">☆</label>
                <input name="note" id="e4" value="4" type="radio">
                <label for="e4">☆</label>
                <input name="note" id="e3" value="3" type="radio">
                <label for="e3">☆</label>
                <input name="note" id="e2" value="2" type="radio">
                <label for="e2">☆</label>
                <input name="note" id="e1" value="1" type="radio">
                <label for="e1">☆</label>
            </div>
            @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif

            <button class="btn btn-outline-info btn-rounded btn-block z-depth-0 my-4 waves-effect" type="submit">Poster mon expérience</button>

        </form>
    </div>
</div>

@endsection
