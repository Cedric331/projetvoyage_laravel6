<footer class="page-footer font-small purple py-4 white-text">
    <div class="container">
        <div class="row justify-content-around">
            <form class="form-inline my-2 my-lg-0" method="POST" action="{{ route('pays.search') }}">
                @csrf
                <input class="form-control mr-sm-2 @error('recherche') is-invalid @enderror" type="search"
                    placeholder="Rechercher un pays" name="recherche" aria-label="Search">
                <button class="btn btn-outline-primary my-2 my-sm-0" type="submit">Rechercher</button>
                @error('recherche')
                <span class="invalid-feedback d-inline" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
                @enderror
            </form>
            <ul class="list-unstyled d-flex mb-0 mt-2">
                <li class="col-m-4">
                    <a class="mr-4" href="{{ route('cgu') }}">Conditions générales</a>
                </li>
                <li class="col-m-4">
                    {{-- <a class="mr-4" href="#!">À propos</a> --}}
                </li>
                <li class="col-m-4">
                    <a class="mr-4" href="{{ route('contact') }}">Nous contacter</a>
                </li>
            </ul>
            <div class="">
                <p class="mt-2 font-weight-bold mb-0">©2020 Copyright IdéeDeVoyage.fr</p>
            </div>
        </div>
    </div>
</footer>
