<header class="sticky-top">
    <nav class="menu">

    <div class="navbar navbar-expand-lg navbar-1 navbar-light d-inline-flex col-4 col-md-8">
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
            aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Menu navigation') }}">
            <span class="navbar-toggler-icon"></span>
        </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav">
                    <li class="nav-item">
                        <a class="navbar-brand text-white" href="{{ route('accueil') }}">
                            IdéeDeVoyage
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link text-white" href="{{ route('liste') }}">Liste des Pays</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link text-white" href="{{ route('annonce') }}">Annonces</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link text-white" href="{{ route('comparateur') }}">Comparateur de Vol</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link text-white" href="{{ route('airbnb') }}">Trouver un Airbnb</a>
                    </li>
                </ul>
            </div>
        </div>

            <div class="float-right d-inline p-2 mr-2" style="max-height:45px;">
                <ul class="ml-auto row">
                @guest
                        <li class="nav-item">
                            <a class="nav-link text-white" href="{{ route('login') }}">{{ __('Se connecter') }}</a>
                        </li>
                        @if (Route::has('register'))
                            <li class="nav-item">
                                <a class="nav-link text-white" href="{{ route('register') }}">{{ __('Inscription') }}</a>
                            </li>
                        @endif

                @else
                    <li class="dropdown nav-item">
                        <a class="btn text-white" href="#" role="button" data-toggle="dropdown"
                            aria-haspopup="true" aria-expanded="false" v-pre>
                            <i class="fas fa-user-friends fa-lg"></i>
                            <span class="badge rounded-pill bg-danger"
                                style="font-size: 10px;">@include('auth.demandeAmis-count')</span>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right m-auto">
                            <a class="dropdown-item centre"
                                href="{{ route('compte-amis') }}">{{ __('Amis') }}</a>
                                <span class="badge rounded-pill bg-danger"
                                style="font-size: 10px;">@include('messenger.unread-count')</span>
                        </div>
                    </li>

                    <li class="dropdown nav-item">
                        <a class="btn text-white" href="#" role="button" data-toggle="dropdown"
                            aria-haspopup="true" aria-expanded="false" v-pre>
                            <i class="fas fa-envelope fa-lg"></i>
                            <span class="badge rounded-pill bg-danger"
                                style="font-size: 10px;">@include('messenger.unread-count')</span>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right m-auto">
                            <a class="dropdown-item centre"
                                href="{{ route('messages') }}">{{ __('Messagerie') }}</a>
                        </div>
                    </li>


                    <li class="dropdown nav-item">
                        <a class="btn text-white" href="#" role="button" data-toggle="dropdown"
                            aria-haspopup="true" aria-expanded="false" v-pre>
                            <i class="fas fa-bell fa-lg"></i>
                            @if (Auth::user()->unreadNotifications->count() > 0)
                            <span class="badge rounded-pill bg-danger" style="font-size: 10px;">
                                {{ Auth::user()->unreadNotifications->count() }}
                            </span>
                            @endif
                        </a>

                        @if (Auth::user()->unreadNotifications->isEmpty())
                            <div class="dropdown-menu dropdown-menu-right">
                                <p class="dropdown-item my-auto">Aucune notification</p>
                            </div>
                        @else

                        <div class="dropdown-menu dropdown-menu-right" id="notification">
                    @foreach (Auth::user()->unreadNotifications as $notification)
                        @if ($notification->type == "App\Notifications\CommentPosted")
                            <a class="dropdown-item"
                                href="{{ route('annonce.view.notification', [$notification->data['annonceId'], $notification->id]) }}">
                                {{ $notification->data['userName']}} à posté un commentaire sur votre annonce

                                <strong>{{ $notification->data['annonceTitle'] }}</strong>

                                <p class="ml-2" style="font-size:13px; color:grey;">
                                    {{ $notification->created_at->diffForHumans() }}
                                </p>
                            </a>

                        @elseif($notification->type == "App\Notifications\CommentaireLike" && $notification->data['type'] == "App\Annonces")
                            <a class="dropdown-item"
                                href="{{ route('commentaireAnnonce.notification', [$notification->data['element'], $notification->id]) }}">
                                {{ $notification->data['userName']}} aime votre commentaire
                                <p class="ml-2" style="font-size:13px; color:grey;">
                                    {{ $notification->created_at->diffForHumans() }}
                                </p>
                            </a>

                        @elseif($notification->type == "App\Notifications\CommentaireLike" && $notification->data['type'] == "App\Pays")
                            <a class="dropdown-item"
                                href="{{ route('commentairePays.notification', [$notification->data['element'], $notification->id]) }}">
                                {{ $notification->data['userName']}} aime votre commentaire
                                <p class="ml-2" style="font-size:13px; color:grey;">
                                    {{ $notification->created_at->diffForHumans() }}
                                </p>
                            </a>

                        @else
                            <a class="dropdown-item"
                                href="{{ route('compte.amis.notification', $notification->id) }}">
                                {{ $notification->data['userName']}} vous a envoyé(e) une demande d'amis
                                <p class="ml-2" style="font-size:13px; color:grey;">
                                    {{ $notification->created_at->diffForHumans() }}
                                </p>
                            </a>
                        @endif
                    @endforeach

                            <form action="{{ route('notification.read') }}" method="POST" class="centre">
                                @csrf
                                <button type="submit" class="btn btn-outline-info waves-effect">
                                    Marqué comme lue
                                </button>
                            </form>
                        </div>
                    </li>

                @endif

                    <li class="nav-item dropdown mt-0 mr-1">
                        <a class=" dropdown-toggle text-white" href="#" role="button"
                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre style="max-height:40px;">
                            <?php
                                $oldfileexists = Storage::disk('public')->exists(Auth::user()->avatar);
                            ?>
                            @if($oldfileexists)
                            <img src="/storage/{{ Auth::user()->avatar }}" alt="avatar du profil" class="avatar">
                            @else
                            {{-- Generation de l'avatar sur https://eu.ui-avatars.com/ --}}
                            <img src="https://eu.ui-avatars.com/api/?size=40&background={{ Auth::user()->color }}&rounded=true&name={{ Auth::user()->pseudo }}"
                                alt="avatar du profil">
                            @endif
                        </a>
                        <div class="dropdown-menu dropdown-menu-right">
                            <a class="dropdown-item" href="{{ route('compte') }}">Mon compte</a>
                            <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();
                                             document.getElementById('logout-form').submit();">
                                {{ __('Déconnexion') }}
                            </a>
                        </div>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>
                    </li>
        @endguest
            </ul>
        </div>
    </nav>
</header>
