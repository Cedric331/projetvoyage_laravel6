<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="Description"
        content="Site pour trouver des idées pour vos prochains voyages, découvrir des destinations ainsi que des compagnons de voyage">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="icon" type="image/png" href="{{ asset('favicon.ico') }}"/>

    <title>{{ config('app.name', 'IdéeDeVoyage') }}</title>

    <!-- Bootstrap / CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css"
        integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/background.css') }}" rel="stylesheet">
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">
    @notifyCss
    @livewireStyles
</head>

<body id="@yield('fond')">

    @include('layouts.nav')


    @if(session()->has('message'))
    <div class="container w-25 alert alert-success">
        {{ session()->get('message') }}
    </div>
    @endif



    <div id="contents">
        @include('notify::messages')
        @yield('content')
    </div>



    <div>
        @include('cookieConsent::index')
    </div>

    @include('layouts.footer')


        {{-- script --}}
    <script src="https://kit.fontawesome.com/a4afbd952e.js" crossorigin="anonymous" defer></script>
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"
        integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous" defer>
    </script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
        integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous" defer>
    </script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"
        integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous" defer>
    </script>
        @notifyJs
        @livewireScripts
    <noscript>Attention pour le bon fonctionnement du site, vous devez activer le JavaScript!</noscript>
</body>

</html>
