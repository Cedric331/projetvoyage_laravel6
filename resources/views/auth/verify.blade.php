@extends('layouts.app')
@section('fond', 'register')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card cadre">
                <div class="card-header">{{ __('Vérifier votre compte e-mail') }}</div>

                <div class="card-body">
                    @if (session('resent'))
                        <div class="alert alert-success" role="alert">
                            {{ __('Un nouveau lien de vérification a été envoyé à votre adresse e-mail') }}
                        </div>
                    @endif
                    <p class="text-dark">{{ __('Pour terminer votre inscription, veuillez vérifier votre messagerie et valider le lien de vérification') }}</p>

                    <form class="d-inline" method="POST" action="{{ route('verification.resend') }}">
                        @csrf
                        <p class="text-dark">{{ __('Si vous n\'avez pas reçu l\'e-mail, cliquer sur le lien pour en recevoir un nouveau') }}</p>
                        <button type="submit" class="btn btn-link text-primary p-0 m-0 align-baseline">{{ __('
                            Renvoyer l\'email') }}</button>.
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
