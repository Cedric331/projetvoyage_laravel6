@extends('layouts.app')
@section('fond', 'compte')
@section('content')


<div class="container my-5">
    <section class="dark-grey-text mb-5">
      @foreach ($experiences as $experience)
      <div class="media avis mt-2 p-4 cadre">
         {!! verificationAvatar(Auth::user()) !!}

        <div class="media-body">
            {!! lienCompte($experience) !!}
            <h5 class="font-weight-bold">{{ $experience->pseudo }}</h5>
          </a>
          <h3><a class="text-dark" href="{{ route('pays.pays', $experience->nom_pays) }}">{{ $experience->nom_pays }}</a></h3>
          <div class="card-data">
            <ul class="list-unstyled mb-1">
              <li class="font-small"><strong>{{ $experience->titre_experience }}</strong></li>
              <li>
                @for ( $i = 0;  $i < $experience->note_experience; $i++)
                    <strong class="note">☆</strong>
                @endfor
            </li>
            </ul>
          </div>
          <p class="article">{{ $experience->description_experience }}</p>
          <a href="{{ route('partage-updateExp',  Crypt::encrypt($experience->id)) }}" class="btn btn-primary m-auto">Modifier mon expérience</a>
        </div>
      </div>
      @endforeach
      </section>
  </div>

@endsection
