@extends('layouts.app')

@section('fond', 'compte')

@section('content')

<div class="container my-5">
    <div class="row justify-content-center">
        <div class="col-md-6 col-sm-12 ">
            <div class="card cadre">
                <div class="card-header">
                    <h1 class="reset text-dark">{{ __('Réinitialiser votre mot de passe') }}</h1>
                </div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ __("Email de réinitialisation envoyé") }}
                        </div>
                    @endif

                    <form method="POST" action="{{ route('password.email') }}">
                        @csrf

                        <div class="form-group">

                            <div>
                                <label for="email" class="col-form-label">{{ __('Adresse email : ') }}</label>
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="@if(Auth::check()){{  Auth::user()->email }} @else {{ old('email') }} @endif" required autocomplete="email" autofocus>

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="offset-md-4 m-auto">
                                <button type="submit" class="col-md-12 btn btn-primary">
                                    {{ __('Réinitialiser') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
