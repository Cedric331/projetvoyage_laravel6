@extends('layouts.app')
@section('fond', 'compte')
@section('content')

<!-- Modal confirmation supprimer compte -->
<div class="modal fade" id="confirmDeleteCompte" tabindex="-1" role="dialog" aria-labelledby="ModelSupprimerCompte"
    aria-hidden="true">
    <div class="modal-dialog modal-notify" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <p class="heading lead">Supprimer votre compte</p>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true" class="white-text">&times;</span>
                </button>
            </div>
            <div class="modal-footer justify-content-center">
                <form action="{{ route('compte.supprimer', Auth::user()->id) }}" method="POST" class="w-50 centre">
                    @csrf
                    @method('DELETE')
                    <button type="submit" class=" btn btn-danger" value="Supprimer">Supprimer mon compte</button>
                </form>
                <a type="button" class="btn btn-success waves-effect" data-dismiss="modal">Non, merci</a>
            </div>
        </div>
    </div>
</div>
<!-- Fin Modal confirmation supprimer compte -->



<div class="container">
    <div class="card mb-6 p-3 centre cadre col-12">
        <h1 class="card-title font-weight-bold">Compte de {{ Auth::user()->pseudo }}</h1>

        @if (Auth::user()->isAdmin())
        <a class="w-50 btn btn-success my-2" href="/gestion/admin">Compte Administrateur</a>
        @endif
        <a class="w-50 btn btn-primary my-2" href="{{ route('compte-amis') }}">Mes amis</a>
        @if ($experiences->count() > 0)
            <a class="w-50 btn btn-primary my-2" href="{{ route('mes-experiences') }}">Voir mes expériences</a>
        @endif
        <a class="w-50 btn btn-primary my-2" href="{{ route('compte.pseudo') }}">Modifier mon pseudo</a>
        <a class="w-50 btn btn-primary my-2" href="{{ route('compte-email') }}">Modifier mon adresse email</a>
        <a class="w-50 btn btn-primary my-2" href="{{ route('password.update') }}">Modifier mon mot de passe</a>
        <a class="w-50 btn btn-primary my-2" href="{{ route('notification') }}">Voir mes notifications</a>


            <button type="button" class="my-2 btn btn-danger" data-toggle="modal"
            data-target="#confirmDeleteCompte" value="Supprimer">Supprimer mon compte</button>
    </div>



    <div class="mb-5 p-3 cadre justify-content-around" style="max-width: 600px;">
        <h1 class="font-weight-bold centre">Avatar du compte</h1>
    <hr>
        <div class="centre col-12">

            <?php
                $oldfileexists = Storage::disk('public')->exists(Auth::user()->avatar);
            ?>
        <div class="col-4">
            @if ($oldfileexists)
            <div>
                <img src="storage/{{ Auth::user()->avatar }}" alt="avatar du profil" class="avatarCompte">
            </div>
            @else
            <img class="d-flex mr-3"
                src="https://eu.ui-avatars.com/api/?size=100&background={{Auth::user()->color}}&rounded=true&name={{Auth::user()->pseudo}}"
                alt="avatar du profil">
            @endif
        </div>

        <div class="col-8">
        @if ($oldfileexists)
        <form action="{{ route('avatar.delete')}}" method="POST" class="centre row"
            enctype="multipart/form-data">
            @method('DELETE')
            @csrf
            <button class="btn btn-danger mt-3 col-6" type="submit">Supprimer mon avatar</button>
        </form>
        @else
                <h3 class="d-flex mr-3 col-12">Modifier la couleur:</h3>
            {{-- Modification de la couleur de l'avatar --}}
            <form action="{{ route('avatar.color') }}" method="POST">
                @method('PATCH')
                @csrf
                <select name="color" class="form-select form-select-lg col-8 mb-2">
                    <option value="defaut">
                        Par défaut
                    </option>
                    <option value="bleu">
                        Bleu
                    </option>
                    <option value="rouge">
                        Rouge
                    </option>
                    <option value="jaune">
                        Jaune
                    </option>
                    <option value="vert">
                        Vert
                    </option>
                </select>
                <button type="submit" class="col-8 btn btn-success">Modifier la couleur</button>
            </form>
        @endif
        </div>
    </div>

        <hr>

        <form action="{{ route('avatar.update')}}" method="POST" class="centre row d-flex justify-content-around"
            enctype="multipart/form-data">
            @method('PATCH')
            @csrf
            <div class="md-form mt-2">
                <input id="avatar" type="file" class="form-control @error('avatar') is-invalid @enderror" name="avatar"
                    aria-describedby="avatar">
                @error('avatar') <span class="error">{{ $message }}</span> @enderror
            </div>
            <button class="btn btn-primary col-4 mt-2" type="submit">Mettre à jour</button>
        </form>
    </div>
</div>

<div class="container pt-5 my-5 z-depth-1 cadre">
    <section class="p-md-3 mx-md-5 text-lg-left">
        <h2 class="text-center font-weight-bold mb-4 pb-1">Mes Favoris</h2>
        <hr class="my-4">
        <div class="row">
            @if(count($favoris) != 0)
            @foreach ($favoris as $pays)
            <div class="col-lg-4 col-sm-6 my-5">
                <div class="row d-flex align-items-center">
                    <div class="col-5 avatar w-100 white d-flex justify-content-center align-items-center">
                        <img src="{{ $pays->image_principal }}"
                            class="img-fluid rounded-circle z-depth-1" />
                    </div>
                    <div class="col-7">
                        <h6 class="font-weight-bold pt-2"></h6>
                        <h4 class="text-muted">
                         <a class="text-dark" href="{{ route('pays.pays',  $pays->nom_pays) }}">
                            {{ $pays->nom_pays }}
                        </a>
                        </h4>
                        <form method="POST" action="{{ route('delete.favoris', $pays->id) }}">
                            @method('DELETE')
                            @csrf
                            <button type="submit" class="btn btn-outline-danger">Supprimer des favoris</button>
                        </form>
                    </div>
                </div>
            </div>
            @endforeach
            @else
            <p class="m-auto font-weight-bold mb-4 pb-1">Aucun favoris dans la liste</p>
            @endif
        </div>
    </section>
</div>
@endsection
