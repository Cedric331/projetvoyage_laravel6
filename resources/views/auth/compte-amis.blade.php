@extends('layouts.app')
@section('fond', 'compte')
@section('content')


<div class="container justify-content m-auto" style="max-width: 500px;">

        @if (count($demandes) != 0)
        <ul class="list-group list-group-flush cadre">
            <ul class="list-group">
            <h2>Demande d'amis en cours</h2>
            @foreach ($demandes as $demande)
            <hr>
                <li class="list-group-item list-group-item-default d-flex justify-content-between">
                    <form action="{{ route('amis.accepter', $demande->id) }}" method="POST">
                        @csrf
                        <input type="submit" class="btn btn-success" value="Accepter">
                    </form>
                    <a href="{{ route('voir-compte', $demande->pseudo ) }}" class="text-info">{{ $demande->pseudo }}</a>
                    <form action="{{ route('amis.refuser', $demande->id ) }}" method="POST">
                        @method('DELETE')
                        @csrf
                        <input type="submit" class="btn btn-danger" value="Refuser">
                    </form>
                </li>
            <hr>
            @endforeach
        </ul>
    </ul>
        @endif



<ul class="list-group list-group-flush cadre">
    <ul class="list-group">
        <h2 class="m-auto">Chercher des utilisateurs</h2>
        <div class="m-auto">
            <div class="m-3 d-flex border border-dark">
                @livewire('search-user')
            </div>
        </div>
        <hr>
        <h2 class="centre">Mes amis</h2>
        @if ($mesAmis->count() == 0)
            <span class="centre mt-2">Aucun ami dans la liste</span>
        @else
            @foreach ($mesAmis as $amis)
            <hr>
            <li class="list-group-item list-group-item-default d-flex justify-content-between">
                {!! verificationAvatar($amis) !!}
                <a href="{{ route('voir-compte', $amis->pseudo ) }}" class="text-info mt-2"><strong>{{ $amis->pseudo }}</strong></a>
                <a href="{{ route('messages.create') }}" class="btn btn-outline-info waves-effect">Envoyer un message</a>
                <form action="{{ route('amis-supprimer', $amis->pseudo ) }}" method="POST">
                    @method('DELETE')
                    @csrf
                    <input type="submit" class="btn btn-outline-danger waves-effect" value="Supprimer">
                </form>
            </li>
            <hr>
            @endforeach
        @endif
    </ul>
</ul>

</div>



@endsection
