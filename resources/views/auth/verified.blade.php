@extends('layouts.app')
@section('fond', 'register')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card cadre">
                <div class="card-header">{{ __('Félicitation') }}</div>

                <div class="card-body">
                        <div class="alert alert-success" role="alert">
                            <p>{{ "Votre compte est maintenant activé" }}</p>
                        </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
