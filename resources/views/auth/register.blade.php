@extends('layouts/app')
@section('fond', 'register')
@section('content')

    <section>
      <div class="mask d-flex justify-content-center align-items-center">
        <div class="container py-5 my-5">
          <div class="row d-flex align-items-center justify-content-center">
              <div class="card cadre">
                <div class="card-body z-depth-2 px-4">
                    <h1 class="centre text-dark">Inscription</h1>
                    <hr>


                <form method="POST" action="{{ route('register') }}">
                @csrf

                @livewire('register')

                <div class="custom-control custom-checkbox mb-2">
                    <input type="checkbox" class="custom-control-input" name="cgu" id="cgu" class="form-check-input @error('cgu') is-invalid @enderror">
                    <label class="custom-control-label" for="cgu">J'accepte les <a class="text-primary" href="{{ route('cgu') }}" target="_blank">conditions générales d'utilisation</a></label>
                    @error('cgu')
                    <span class="invalid-feedback" role="alert">
                        <strong>Merci de valider les conditions générales d'utilisation</strong>
                    </span>
                    @enderror
                </div>

                <div class="md-form my-4">
                    {!! NoCaptcha::display() !!}
                    @if ($errors->has('g-recaptcha-response'))
                    <span class="help-block" style="color:rgb(201, 45, 45);">
                        <strong>{{ 'Merci de valider le captcha' }}</strong>
                    </span>
                    @endif
                </div>

                  <div class="text-center mt-3 my-3">
                    <button id="bouton" class="btn btn-primary btn-block">
                        {{ __('S\'inscrire') }}
                    </button>
                </form>
                  </div>
                </div>
              </div>
          </div>
        </div>
      </div>
    </section>

  {!! NoCaptcha::renderJs() !!}

@endsection
