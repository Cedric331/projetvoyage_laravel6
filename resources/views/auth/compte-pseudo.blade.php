@extends('layouts.app')
@section('fond', 'compte')
@section('content')

<div class="container mt-5">
    <div class="card mb-6 p-3 m-auto cadre text-white">
          <div><h1 class="card-title text-dark centre">Modification pseudo</h1></div>
          <hr>
          <p class="card-text m-auto text-dark">Pseudo : {{ Auth::user()->pseudo }}</p>
          <form action="{{ route('compte.pseudo.update') }}" method="POST" class="m-auto">
            @method('PATCH')
            @csrf
              <div>
                <label for="pseudo">Pseudo:</label>
                <input type="text" class="@error('pseudo') is-invalid @enderror" value="{{ old('pseudo') }}" id="pseudo" name="pseudo">
                @error('pseudo')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
                @enderror
              </div>
              <hr>
              <div class="centre">
                  <button type="submit" class="btn btn-primary">Valider</button>
              </div>
          </form>
    </div>
</div>

@endsection
