<?php

use TCG\Voyager\Facades\Voyager;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route générales

// Page d'accueil
Route::view('/','accueil');

// Affiche page d'accueil
Route::view('accueil', 'accueil')->name('accueil');

// Recherche du menu d'accueil
Route::post('accueil/recherche', 'AccueilController@searchPays')->name('search.pays');

// Recherche aléatoire
Route::post('/accueil/random/recherche', 'AccueilController@searchRandom')->name('search.pays.random');

// Affiche les cgu
Route::view('cgu', 'cgu')->name('cgu');

// Route Pays
Route::get('/liste', 'PaysController@liste')->name('liste');

// Affiche le pays sélectionné
Route::get('/pays/{nomPays}', 'PaysController@affichePays')->name('pays.pays');

// Affiche le Comparateur de Vol
Route::get('/comparateur', 'ComparateurController@indexComparateur')->name('comparateur');

// Affiche le Comparateur de Vol
Route::get('/airbnb', 'ComparateurController@indexAirbnb')->name('airbnb');

// Recherche de Pays
Route::post('/recherche', 'SearchController@search')->name('pays.search');

// Affiche le formulaire de contact
Route::view('/contact', 'contact.contact')->name('contact');

// Affiche le formulaire de contact
Route::post('/contact', 'ContactController@store')->name('contact.post');

// Affiche page du annonce
Route::get('annonce', 'AnnoncesController@annonceIndex')->name('annonce');

// Authentification
Auth::routes(['verify' => true]);

// Route authentifié
Route::group([
    'middleware' => 'auth',
], function () {

// Route Admin
Route::group(['prefix' => 'gestion/admin', 'middleware' => 'admin',], function () {
    Voyager::routes();
});

    // Affiche page du compte
    Route::get('compte', 'CompteController@index')->name('compte');

    // Supprimer le compte de l'utilisateur
    Route::delete('compte/supprimer', 'CompteController@destroy')->name('compte.supprimer');

    // Affiche le formulaire pour mettre à jour l'adresse email
    Route::get('compte/email', 'CompteController@emailUpdateForm')->name('compte-email');

    // Mettre à jour l'adresse email
    Route::patch('compte/email', 'CompteController@emailUpdate')->name('compte.email');

    // Affiche le formulaire pour mettre à jour le pseudo
    Route::get('compte/pseudo', 'CompteController@pseudo')->name('compte.pseudo');

    // Envoi le formulaire pour mettre à jour le pseudo
    Route::patch('compte/pseudo', 'CompteController@pseudoUpdate')->name('compte.pseudo.update');

// Route Vérifié
Route::group(['middleware' => 'verified',], function () {

    // Upload de l'Avatar
    Route::patch('compte/avatar', 'CompteController@avatarUpdate')->name('avatar.update')->middleware('image-sanitize');

    // Modifie la couleur de l'avatar
    Route::patch('compte/avatar/color', 'CompteController@avatarColor')->name('avatar.color');

    // Supprime l'avatar
    Route::delete('compte/avatar', 'CompteController@avatarDelete')->name('avatar.delete');

    // Voir les notifications
    Route::get('compte/notification', 'CompteController@indexNotification')->name('notification');

    // Notification lue
    Route::post('/notification', 'CompteController@notificationRead')->name('notification.read');

    //Voir le compte d'un autre utilisateur
    Route::get('/voir-compte/{pseudo}', 'AmisController@voirCompte')->name('voir-compte');

    // Demande d'ajout d'un ami
    Route::post('/voir-compte/ajouter/{id}', 'AmisController@ajouterAmis')->name('ajouter.amis');

    // Annule la demande d'amis
    Route::delete('/voir-compte/delete/{id}', 'AmisController@deleteDemande')->name('delete.demande');

    // Affiche la page amis du comtpe
    Route::get('/compte/amis', 'AmisController@indexAmis')->name('compte-amis');

    // Affiche la page amis du comtpe depuis la notification
    Route::get('/compte/amis/{notification}', 'AmisController@indexAmisNotification')->name('compte.amis.notification');

    // Affiche la page des expériences
    Route::get('/compte/mes-experiences', 'CompteController@mesExperiences')->name('mes-experiences');

    //Demande d'amis rejetée
    Route::delete('/compte/amis/refuser/{id}', 'AmisController@refuser')->name('amis.refuser');

    //Demande d'amis acceptée
    Route::post('/compte/amis/accepter/{id}', 'AmisController@accepter')->name('amis.accepter');

    // Supprime un amis
    Route::delete('/compte/amis/{id}/supprimer', 'AmisController@supprimerAmis')->name('amis-supprimer');

    // Formulaire pour partager l'expérience sur un pays
    Route::get('/partage/{nomPays}', 'ExperienceController@form')->name('partage');

    // Poster l'expérience sur un Pays
    Route::post('/partage/{nomPays}', 'ExperienceController@store')->name('partage-store');

    // Formulaire pour modifier l'expérience sur un pays
    Route::get('/partage/experience/{idExperience}', 'ExperienceController@getForm')->name('partage-updateExp');

    // Formulaire pour mettre à jour l'expérience sur un pays
    Route::put('/partage/experience/update/{idExperience}', 'ExperienceController@update')->name('partage-update');

    // Supprimer l'expérience
    Route::delete('/partage/experience/delete/{idExperience}', 'ExperienceController@delete')->name('partage-delete');

    // Affiche page du formulaire
    Route::get('annonce/formulaire', 'AnnoncesController@annonceFormulaire')->name('annonceFormulaire');

    // Envoi du formulaire d'annonce
    Route::post('annonce/formulaire', 'AnnoncesController@annoncePost')->name('annonceFormulaire.post');

    // Affiche la page de l'annonce
    Route::get('annonce/view/{idAnnonce}', 'AnnoncesController@annonceView')->name('annonce.view');

    // Affiche la page de l'annonce depuis la notification
    Route::get('annonce/view/{idAnnonce}/{notficationId}', 'AnnoncesController@annonceViewNotification')->name('annonce.view.notification');

    // Affiche le formulaire de modification de l'annonce
    Route::get('annonce/formulaire/update/{idAnnonce}', 'AnnoncesController@annonceUpdate')->name('annonce.update');

    // Modification de l'annonce
    Route::patch('annonce/formulaire/update/{idAnnonce}', 'AnnoncesController@annoncePatch')->name('annonce.update.patch');

    // Supprime l'annonce
    Route::delete('annonce/formulaire/delete/{idAnnonce}', 'AnnoncesController@annonceDelete')->name('annonce.delete');

    // Ajoute un pays dans la liste des favoris
    Route::post('/pays/{nomPays}', 'FavorisController@addFavoris')->name('pays.favoris');

    // Supprime un pays dans la liste des favoris
    Route::delete('/pays/{nomPays}', 'FavorisController@deleteFavoris')->name('delete.favoris');

    // Accède au commentaire liké depuis la notification
    Route::get('annonce/view/{idAnnonce}/{notification}', 'AnnoncesController@annonceNotification')->name('commentaireAnnonce.notification');

    // Accède au commentaire liké depuis la notification
    Route::get('/pays/{pays}/{notification}', 'PaysController@paysNotification')->name('commentairePays.notification');

    // Retourne la view vérification
    Route::get('/verification', function () {
        return view('auth.verified');
    });

    // Route messagerie
    Route::group(['prefix' => 'messages'], function () {
        Route::get('/', ['as' => 'messages', 'uses' => 'MessagesController@index']);
        Route::get('create', ['as' => 'messages.create', 'uses' => 'MessagesController@create']);
        Route::post('/', ['as' => 'messages.store', 'uses' => 'MessagesController@store']);
        Route::get('{id}', ['as' => 'messages.show', 'uses' => 'MessagesController@show']);
        Route::put('{id}', ['as' => 'messages.update', 'uses' => 'MessagesController@update']);
        Route::patch('{id}/{userId}', ['as' => 'messages.leave', 'uses' => 'MessagesController@leave']);
        Route::delete('{id}', ['as' => 'messages.delete', 'uses' => 'MessagesController@delete']);
    });
});

});

// Page Erreur

Route::fallback(function() {
    return view('errors.404');
    return view('errors.419');
    return view('errors.500');
 });





