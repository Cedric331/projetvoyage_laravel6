<?php

namespace Tests\Feature;

use App\Pays;
use App\User;
use App\Annonces;
use App\Experience;
use Tests\TestCase;
use DatabaseMigrations;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class AuthTest extends TestCase
{

    /**
     * @test
     *
     * Vérifie si un utilisateur non authentifié est bien redirigé sur la page login
     * en essayant d'accéder à une page qui nécessite d'être authentifié
     */
    public function UnauthentifiedCompte()
    {
        $response = $this->get('compte');
        $response->assertStatus(302);
        $response->assertRedirect('/login');
    }

    /**
     * @test
     *
     * Vérifie si un utilisateur non authentifié est bien redirigé sur la page login
     * en essayant d'accéder à une page qui nécessite d'être authentifié
     */
    public function UnauthentifiedAmis()
    {
        $response = $this->get('compte/amis');
        $response->assertStatus(302);
        $response->assertRedirect('/login');
    }

    /**
     * @test
     *
     * Vérifie si un utilisateur non authentifié est bien redirigé sur la page login
     * en essayant d'accéder à une page qui nécessite d'être authentifié
     */
    public function UnauthentifiedMessage()
    {
        $response = $this->get('messages');
        $response->assertStatus(302);
        $response->assertRedirect('/login');
    }

}
