<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class TestRoute extends TestCase
{
    /**
     * @test
     * Test la route accueil
     * @return void
     */
    public function testRouteAccueil()
    {
        $response = $this->get('accueil');
        $response->assertStatus(200);
    }

   /**
     * @test
     * Test la route pays
     * @return void
     */
    public function testRoutePays()
    {
        $response = $this->get('pays');
        $response->assertStatus(200);
    }

}
