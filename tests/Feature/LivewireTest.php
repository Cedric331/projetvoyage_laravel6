<?php

namespace Tests\Feature;

use App\Pays;
use App\User;
use Tests\TestCase;
use Livewire\Livewire;
use App\Commentaires;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class LivewireTest extends TestCase
{
    /**
     * @test
     * Fonction test pour poster un commentaire sur la page pays
     */
    function can_create_commentaire()
    {
        $user = User::find(1);
        $pays = Pays::first();

        Livewire::actingAs($user);

        Livewire::test('commentaire', ['page' => $pays->id, 'type' => get_class($pays)])
            ->set('message', 'Test commentaire')
            ->call('save');

        $this->assertTrue(Commentaires::where('message','Test commentaire')->exists());
    }

    /**
     * @test
     * Fonction test refus de poster un commentaire avec un utilisateur non authentifié
     */
    function cant_create_commentaire()
    {
        $pays = Pays::first();

        $response = Livewire::test('commentaire', ['page' => $pays->id, 'type' => get_class($pays)])
            ->set('message', 'Refus')
            ->call('save');

        $response->assertStatus(403);
    }

    /**
     * @test
     * Fonction test pour éditer un commentaire
     */
    function can_edit_commentaire()
    {

        $user = User::find(1);
        $pays = Pays::first();
        $commentaire = Commentaires::first();

        Livewire::actingAs($user);

        Livewire::test('commentaire', ['page' => $pays->id, 'type' => get_class($pays)])
            ->set(['commentaire' => $commentaire, 'editCommentaire' => 'Test commentaire modifié'])
            ->call('edit');

        $this->assertTrue(Commentaires::where('message','Test commentaire modifié')->exists());
    }

    /**
     * @test
     * Fonction test qui doit refuser d'éditer un commentaire qui n'appartient pas à l'utilisateur
     */
    function cant_edit_commentaire()
    {
        $user = User::find(8);
        $pays = Pays::first();
        $commentaire = Commentaires::first();

        Livewire::actingAs($user);

        $response = Livewire::test('commentaire', ['page' => $pays->id, 'type' => get_class($pays)])
            ->set(['commentaire' => $commentaire, 'editCommentaire' => 'Test commentaire non autorisé'])
            ->call('edit');

        $response->assertStatus(403);
    }

    /**
     * @test
     * Fonction test pour supprimer un commentaire avec utilisateur n'étant pas le créateur du commentaire
     */
    function cant_delete_commentaire()
    {

        $user = User::find(8);
        $pays = Pays::first();
        $commentaire = Commentaires::first();

        Livewire::actingAs($user);

        $response = Livewire::test('commentaire', ['page' => $pays->id, 'type' => get_class($pays)])
            ->call('delete', $commentaire->id);

        $response->assertStatus(403);
    }

    /**
     * @test
     * Fonction test pour supprimer un commentaire
     */
    function can_delete_commentaire()
    {
        $user = User::find(1);
        $pays = Pays::first();
        $commentaire = Commentaires::where('user_id', $user->id)->first();

        Livewire::actingAs($user);

        Livewire::test('commentaire', ['page' => $pays->id, 'type' => get_class($pays)])
            ->call('delete', $commentaire->id);
    }

    /**
     * @test
     * Vérifie si le component est bien présent sur la page
     */
    function pays_contains_livewire_component()
    {
        $this->get('/pays/france')->assertSeeLivewire('commentaire');
    }

    /**
     * @test
     * Vérifie si le component est bien présent sur la page
     * */
    function register_contains_livewire_component()
    {
        $this->get('/register')->assertSeeLivewire('register');
    }
}
