<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePeriodesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('periodes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('mois_id')->unsigned();
            $table->foreign('mois_id')->references('id')->on('mois');
            $table->unsignedBigInteger('saison_id')->unsigned();
            $table->foreign('saison_id')->references('id')->on('saisons');
            $table->unsignedBigInteger('pays_id')->unsigned();
            $table->foreign('pays_id')->references('id')->on('pays')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('periodes');
    }
}
