<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePaysTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pays', function (Blueprint $table) {
            $table->bigIncrements('id')->index();
            $table->string('nom_pays')->unique();
            $table->integer('prix');
            $table->string('iso');
            $table->string('image_principal');
            $table->longText('description_pays');
            $table->longText('description_climat');
            $table->longText('description_partir');
            $table->unsignedBigInteger('continent_id')->unsigned();
            $table->foreign('continent_id')->references('id')->on('continents');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pays');
    }
}
