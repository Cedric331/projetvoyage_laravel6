<?php

use App\User;
use App\Annonces;
use App\Commentaires;
use Illuminate\Database\Seeder;

class CommentaireAnnonceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        $faker = Faker\Factory::create('fr_FR');
        $users = User::all();
        $array = collect([]);
        foreach ($users as $user)
        {
            $array->push($user->id);
        }

        $annonces = Annonces::all();
        $arrayAnnonce = collect([]);
        foreach ($annonces as $annonce)
        {
            $arrayAnnonce->push($annonce->id);
        }

    for ($i=0; $i < 50; $i++) {
        $commentaire = new Commentaires;
        $commentaire->message = $faker->realText($maxNbChars = 50, $indexSize = 2);
        $commentaire->type = "App\Annonces";
        $commentaire->user_id = $array->random();
        $commentaire->element_id = $arrayAnnonce->random();
        $commentaire->created_at = now();
        $commentaire->save();
    }
}
}
