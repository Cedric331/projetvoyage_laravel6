<?php

use App\User;
use App\Annonces;
use Illuminate\Database\Seeder;

class AnnonceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        $faker = Faker\Factory::create('fr_FR');
        $users = User::all();
        $array = collect([]);
        foreach ($users as $user)
        {
            $array->push($user->id);
        }

    for ($i=0; $i < 5; $i++) {
        $annonce = new Annonces;
        $annonce->titre = $faker->realText($maxNbChars = 10, $indexSize = 2);
        $annonce->description = $faker->realText($maxNbChars = 50, $indexSize = 2);
        $annonce->user_id = $array->random();
        $annonce->pays_id = rand(1,12);
        $annonce->created_at = now();
        $annonce->save();
        }
    }
}
