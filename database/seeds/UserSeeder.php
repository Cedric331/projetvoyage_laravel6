<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\User;
use Illuminate\Support\Str;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{

    public function run() {
            $faker = Faker\Factory::create('fr_FR');

        for ($i=0; $i < 50; $i++) {
            $user = new User;
            $user->pseudo = $faker->unique()->firstName();
            $user->email = $faker->unique()->email;
            $user->email_verified_at = now();
            $user->created_at = now();
            $user->updated_at = now();
            $user->password = Hash::make('password');
            $user->save();
        }
}
}
