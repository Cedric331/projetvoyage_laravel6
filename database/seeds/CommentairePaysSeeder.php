<?php

use App\Pays;
use App\User;
use App\Commentaires;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;
use Illuminate\Database\Seeder;

class CommentairePaysSeeder extends Seeder
{
    public function run() {
        $faker = Faker\Factory::create('fr_FR');
        $users = User::all();
        $array = collect([]);
        foreach ($users as $user)
        {
            $array->push($user->id);
        }

    for ($i=0; $i < 50; $i++) {
        $commentaire = new Commentaires;
        $commentaire->message = $faker->realText($maxNbChars = 50, $indexSize = 2);
        $commentaire->type = "App\Pays";
        $commentaire->user_id = $array->random();
        $commentaire->element_id = rand(1,12);
        $commentaire->created_at = now();
        $commentaire->save();
    }
}
}

