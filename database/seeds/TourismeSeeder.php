<?php

use App\Pays;
use App\Tourismes;
use Illuminate\Support\Str;
use Illuminate\Database\Seeder;

class TourismeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
     public function run()
     {

         for ($i=0; $i < 20; $i++)
         {
            $tourisme = new Tourismes();

                 $tourisme->titre_tourisme = Str::random(10);
                 $tourisme->image_tourisme = Str::random(10);
                 $tourisme->description_tourisme = Str::random(10);
                 $tourisme->lien_tourisme = Str::random(50);
                 $tourisme->pays_id = Pays::all()->random(1)->first()->id;
                 $tourisme->save();
         }
     }
}
